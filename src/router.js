import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import PageNotFound from './views/Docs/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/button',
      name: 'Button',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Button.md')
    },
    {
      path: '/input',
      name: 'Input',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Input.md')
    },
    {
      path: '/label',
      name: 'Label',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Label.md')
    },
    {
      path: '/avatar',
      name: 'Avatar',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Avatar.md')
    },
    {
      path: '/select2',
      name: 'Select2',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Select2.md')
    },
    {
      path: '/select',
      name: 'Select',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Select.md')
    },
    {
      path: '/datepicker',
      name: 'DatePicker',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/DatePicker.md')
    },
    {
      path: '/pagination',
      name: 'Pagination',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Pagination.md')
    },
    {
      path: '/information',
      name: 'Information',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Information.md')
    },
    {
      path: '/switch',
      name: 'Switch',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Switch.md')
    },
    {
      path: '/progress',
      name: 'Progress',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Progress.md')
    },
    {
      path: '/message',
      name: 'Message',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Message.md')
    },
    {
      path: '/modal',
      name: 'Modal',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Modal.md')
    },
    {
      path: '/bread-crumb',
      name: 'BreadCrumb',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/BreadCrumb.md')
    },
    {
      path: '/table',
      name: 'Table',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Table.md')
    },
    {
      path: '/loading',
      name: 'Loading',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Loading')
    },
    {
      path: '/navigation',
      name: 'Navigation',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Navigation.md')
    },
    {
      path: '/tab',
      name: 'Tab',
      component: () => import(/* webpackChunkName: "docs" */ '@/views/Docs/Tab.md')
    },
    {
      path: '/tag',
      name: 'Tag',
      component: () => import(/* webpackChunkName: "docs" */ '@/views/Docs/Tag.md')
    },
    {
      path: '/color',
      name: 'Color',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Color')
    },
    {
      path: '/typography',
      name: 'Typography',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Typography')
    },
    {
      path: '/icons',
      name: 'Icons',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Icons')
    },
    {
      path: '/checkbox',
      name: 'Checkbox',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Checkbox.md')
    },
    {
      path: '/radio',
      name: 'Radio',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Radio.md')
    },
    {
      path: '/layout',
      name: 'Layout',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Layout.md')
    },
    {
      path: '/popover',
      name: 'Popover',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Popover.md')
    },
    {
      path: '/menu',
      name: 'Menu',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Menu.md')
    },
    {
      path: '/tree',
      name: 'Tree',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Tree')
    },
    {
      path: '/slider',
      name: 'Slider',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Slider.md')
    },
    {
      path: '/drawer',
      name: 'Drawer',
      component: () =>
        import(/* webpackChunkName: "docs" */ '@/views/Docs/Drawer.md')
    },
    {
      path: '/playground',
      name: 'Playground',
      component: () =>
        import(/* webpackChunkName: "playground" */ '@/views/Playground')
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '*',
      name: 'NotFound',
      component: PageNotFound
    }
  ]
})
