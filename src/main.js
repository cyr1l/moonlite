import Vue from 'vue'
import App from './App.vue'
import router from './router'
import MoonLite from '../index'
import MlCode from '@/components/Code.vue'
import MlExample from '@/components/Example.vue'
import MlTOC from '@/components/TOC.vue'

Vue.config.productionTip = false
Vue.component('ml-code', MlCode)
Vue.component('ml-example', MlExample)
Vue.component('ml-toc', MlTOC)

MoonLite.install(Vue)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
