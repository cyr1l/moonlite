function findPos (el) {
  var curtop = 0
  var curleft = 0

  if (el.offsetParent) {
    do {
      curleft += el.offsetLeft
      curtop += el.offsetTop
    } while ((el = el.offsetParent))
  }

  return [curleft, curtop]
}

function position (el, absolute) {
  if (absolute) {
    let [left, top] = findPos(el)
    return {
      left,
      top
    }
  } else {
    return { left: el.offsetLeft, top: el.offsetTop }
  }
}

function outerWidth (el) {
  return el.offsetWidth
}

function outerHeight (el) {
  return el.offsetHeight
}

function height (el) {
  return el.offsetHeight
}

function width (el) {
  return el.offsetWidth
}

function addClass (el, className) {
  if (el.classList) {
    el.classList.add(className)
  } else {
    el.className += ' ' + className
  }
}

function removeClass (el, className) {
  if (el.classList) {
    el.classList.remove(className)
  } else {
    el.className = el.className.replace(
      new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'),
      ' '
    )
  }
}

function hasClass (el, className) {
  if (el.classList) {
    return el.classList.contains(className)
  } else {
    return RegExp('(^| )' + className + '( |$)', 'gi').test(el.className)
  }
}

function toggleClass (el, className, toggle) {
  if (typeof toggle !== 'undefined') {
    if (toggle) {
      addClass(el, className)
    } else {
      removeClass(el, className)
    }
  } else {
    if (hasClass(el)) {
      removeClass(el, className)
    } else {
      addClass(el, className)
    }
  }
}

function template (str) {
  var tmp = document.implementation.createHTMLDocument()
  tmp.body.innerHTML = str
  return tmp.body.children
}

function append (el, parent) {
  parent.appendChild(el)
}

function prepend (el, parent) {
  parent.insertBefore(el, parent.firstChild)
}

function remove (el) {
  if (el && el.parentNode) {
    el.parentNode.removeChild(el)
  }
}

export default {
  position,
  outerHeight,
  outerWidth,
  height,
  width,
  addClass,
  removeClass,
  hasClass,
  toggleClass,
  template,
  append,
  prepend,
  remove
}
