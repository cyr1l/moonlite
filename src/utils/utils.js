import cloneDeep from 'lodash/cloneDeep'
import find from 'lodash/find'
import isEqual from 'lodash/isEqual'
import debounce from 'lodash/debounce'
import defaults from 'lodash/defaults'
import isEmpty from 'lodash/isEmpty'
import sortBy from 'lodash/sortBy'
import every from 'lodash/every'
import filter from 'lodash/filter'
import findIndex from 'lodash/findIndex'
import map from 'lodash/map'
import clone from 'lodash/clone'
import groupBy from 'lodash/groupBy'
import first from 'lodash/first'
import last from 'lodash/last'
import merge from 'lodash/merge'
import get from 'lodash/get'
import property from 'lodash/property'
import matchesProperty from 'lodash/matchesProperty'
import includes from 'lodash/includes'
import indexOf from 'lodash/indexOf'
import pullAll from 'lodash/pullAll'

export default {
  cloneDeep,
  find,
  isEqual,
  debounce,
  defaults,
  isEmpty,
  sortBy,
  every,
  filter,
  findIndex,
  map,
  clone,
  groupBy,
  first,
  last,
  merge,
  get,
  property,
  matchesProperty,
  includes,
  indexOf,
  pullAll
}
