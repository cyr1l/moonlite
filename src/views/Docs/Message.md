# Message

## Example: 

### Simple Usage: 
```html run
<template>
  <ml-button type="default" icon="warning-sign" @click="$message('Say hello!')">
    Default Message
  </ml-button>
</template>
```

```html run
<template>
  <div>
    <ml-switch on-text="Heavy" off-text="Not Heavy" v-model="heavy">
      Heavy
    </ml-switch>
    <hr class="ml-hr">
    <ml-row>
      <ml-col>
        <ml-button type="default" icon="warning-sign" @click="sendMessage()">
          Default Message
        </ml-button>
        <ml-button type="primary" icon="warning-sign" @click="sendMessage('primary')">
          Primary Message
        </ml-button>
        <ml-button type="success" icon="warning-sign" @click="sendMessage('success')">
          Success Message
        </ml-button>
        <ml-button type="warning" icon="warning-sign" @click="sendMessage('warning')">
          Warning Message
        </ml-button>
        <ml-button type="danger" icon="warning-sign" @click="sendMessage('danger')">
          Error Message
        </ml-button>
      </ml-col>
    </ml-row>
    <hr class="ml-hr">
    <ml-row>
      <ml-col>
        <ml-button type="default" icon="warning-sign" @click="sendMessage('default', 'help')">
          Help Message
        </ml-button>
        <ml-button type="default" icon="warning-sign" @click="sendMessage('success', 'tick')">
          Check Message
        </ml-button>
        <ml-button type="default" icon="warning-sign" @click="sendMessage('success', 'tick', 0)">
          No Close Message
        </ml-button>
        <ml-button type="default" icon="warning-sign" @click="sendMessageWithAction('success', 'tick', 0)">
          Send Message With Actions
        </ml-button>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    return {
      heavy: false
    }
  },
  methods: {
    sendMessage (type, icon, duration) {
      this.$message({
        message: `Your computer's time zone does not appear to match your Jira time zone preference of (GMT+00:00) UTC.`,
        icon: icon || 'warning-sign',
        type: type,
        heavy: this.heavy,
        duration: duration,
        actions: [
          {
            text: 'Retry',
            icon: 'refresh'
          }
        ]
      })
    },
    sendMessageWithAction () {
      this.$message({
        message: `Your computer's time zone does not appear to match your Jira time zone preference of (GMT+00:00) UTC.`,
        duration: 0,
        type: '',
        heavy: this.heavy,
        actions: [
          {
            text: 'OK',
            icon: 'thumbs-up',
            callback: () => {
              this.$message('OK!')
            }
          },
          {
            type: '',
            text: 'Not OK',
            icon: 'thumbs-down',
            callback: () => {
              this.$message('Not ok!')
            }
          }
        ]
      })
    }
  }
}
</script>
```

<ml-button intent="primary" v-on:click="$message('Hello???')" icon="search">Say Hi</ml-button>
