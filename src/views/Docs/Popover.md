# Popover

Popovers display floating content next to a target element.

`Popover` is built on top of the [**Popper.js**](https://popper.js.org/) library. Popper.js is a small (`~6kb`) library that offers a powerful, customizable positioning engine and operates at blazing speed (`~60fps`).

```html run
<template>
  <div>
    <ml-popover>
      <template slot-scope="scope">
        <div style="width: 400px">
          <p class="ml-text-default align-left ml-text-paragraph">
            Are you sure you want to delete these items? You won't be able to recover them.
          </p>
          <div class="align-right" style="margin-top: 20px;">
            <ml-button type="subtle" focus @click="scope.popover.close()">Cancel</ml-button>
            <ml-button intent="danger" icon="trash" @click="$message('Deleted!'); scope.popover.close();">Delete</ml-button>
          </div>
        </div>
      </template>
      <template slot="target" placement="bottom">
        <ml-button intent="danger" icon="trash">Delete Item</ml-button>
      </template>
    </ml-popover>
    <ml-popover style="margin-left: 10px;" placement="right">
      <template>
        你好，世界。
      </template>
      <template slot="target">
        <ml-button intent="primary">Hello, world.</ml-button>
      </template>
    </ml-popover>
  </div>
</template>
```

Popover with different position:

```html run
<template>
  <div>
    <ml-row class="margin-bottom">
      <ml-col :span="2" :offset="2">
        <ml-popover placement="top-start">
          <template>
            Positin: top-start
          </template>
          <template slot="target">
            <ml-button intent="primary">top-start</ml-button>
          </template>
        </ml-popover>
      </ml-col>
      <ml-col :span="2">
        <ml-popover placement="top">
          <template>
            Positin: top
          </template>
          <template slot="target">
            <ml-button intent="primary">top</ml-button>
          </template>
        </ml-popover>
      </ml-col>
      <ml-col :span="2">
        <ml-popover placement="top-end">
          <template>
            Positin: top-end
          </template>
          <template slot="target">
            <ml-button intent="primary">top-end</ml-button>
          </template>
        </ml-popover>
      </ml-col>
    </ml-row>
    <ml-row class="margin-bottom">
      <ml-col :span="2" class="align-right">
        <ml-popover placement="left-start">
          <template>
            Positin: left-start
          </template>
          <template slot="target">
            <ml-button intent="primary">left-start</ml-button>
          </template>
        </ml-popover>
      </ml-col>
      <ml-col :span="2" :offset="6" class="align-left">
        <ml-popover placement="right-start">
          <template>
            Positin: right-start
          </template>
          <template slot="target">
            <ml-button intent="primary">right-start</ml-button>
          </template>
        </ml-popover>
      </ml-col>
    </ml-row>
    <ml-row class="margin-bottom">
      <ml-col :span="2" class="align-right">
        <ml-popover placement="left">
          <template>
            Positin: left
          </template>
          <template slot="target">
            <ml-button intent="primary">left</ml-button>
          </template>
        </ml-popover>
      </ml-col>
      <ml-col :span="2" :offset="2">
        <ml-popover placement="right">
          <template>
            Position: auto
          </template>
          <template slot="target">
            <ml-button intent="primary">auto</ml-button>
          </template>
        </ml-popover>
      </ml-col>
      <ml-col :span="2" :offset="2" class="align-left">
        <ml-popover placement="right">
          <template>
            Positin: right
          </template>
          <template slot="target">
            <ml-button intent="primary">right</ml-button>
          </template>
        </ml-popover>
      </ml-col>
    </ml-row>
    <ml-row class="margin-bottom">
      <ml-col :span="2" class="align-right">
        <ml-popover placement="left-end">
          <template>
            Positin: left-end
          </template>
          <template slot="target">
            <ml-button intent="primary">left-end</ml-button>
          </template>
        </ml-popover>
      </ml-col>
      <ml-col :span="2" :offset="6" class="align-left">
        <ml-popover placement="right-end">
          <template>
            Positin: right-end
          </template>
          <template slot="target">
            <ml-button intent="primary">right-end</ml-button>
          </template>
        </ml-popover>
      </ml-col>
    </ml-row>
    <ml-row>
      <ml-col :span="2" :offset="2">
        <ml-popover placement="bottom-start">
          <template>
            Positin: bottom-start
          </template>
          <template slot="target">
            <ml-button intent="primary">bottom-start</ml-button>
          </template>
        </ml-popover>
      </ml-col>
      <ml-col :span="2">
        <ml-popover placement="bottom">
          <template>
            Positin: bottom
          </template>
          <template slot="target">
            <ml-button intent="primary">bottom</ml-button>
          </template>
        </ml-popover>
      </ml-col>
      <ml-col :span="2">
        <ml-popover placement="bottom-end">
          <template>
            Positin: bottom-end
          </template>
          <template slot="target">
            <ml-button intent="primary">bottom-end</ml-button>
          </template>
        </ml-popover>
      </ml-col>
    </ml-row>
  </div>
  
</template>
```

Nested Popover:

```html run
<template>
  <div>
    <ml-popover placement="bottom">
      <template slot="target">
        <ml-button intent="primary">Nested Popover</ml-button>
      </template>
      <div class="ml-text-default ml-text-paragraph">
        Nested: level 1
      </div>
      <ml-popover placement="bottom">
        <template slot="target">
          <ml-button intent="primary">Open Another</ml-button>
        </template>
        Nested: level 2
        <ml-popover placement="bottom">
          <template slot="target">
            <ml-button intent="primary">And Another</ml-button>
          </template>
          Nested: level 3
        </ml-popover>
      </ml-popover>
    </ml-popover>

    <ml-popover placement="bottom" prevent style="margin-left: 10px">
      <template slot="target">
        <ml-button intent="primary">with `prevent` prop</ml-button>
      </template>
      <div class="ml-text-default ml-text-paragraph" style="width: 400px;">
        With
        <span class="ml-text-monospace">`prevent`</span> prop, only the top popover will be closed when click in blank place.,
      </div>
      <ml-popover placement="right" prevent>
        <template slot="target">
          <ml-button intent="primary">Open Another</ml-button>
        </template>
        <div class="ml-text-default ml-text-paragraph">
          placement: right
        </div>
        <ml-popover placement="bottom" prevent>
          <template slot="target">
            <ml-button intent="primary">And Another</ml-button>
          </template>
          placement: bottom
        </ml-popover>
      </ml-popover>
    </ml-popover>

    <ml-popover style="margin-left: 10px;" placement="bottom-start">
      <template slot="target">
        <ml-button intent="primary">Popover scope</ml-button>
      </template>
      <template slot-scope="scope">
        <div class="ml-text-default ml-text-paragraph align-left" style="margin-bottom: 15px; width: 400px;">
          With the
          <span class="ml-text-monospace">`popover`</span>
          instance in 
          <span class="ml-text-monospace">`slot-scope`</span>,
          we can close popover maunal with 
          <span class="ml-text-monospace">`close()`</span>
          method.
        </div>
        <div class="align-right">
          <ml-button intent="danger" icon="cross" @click="scope.popover.close()">scope.popover.close()</ml-button>
        </div>
      </template>
    </ml-popover>
  </div>
</template>
```

**Popover appearance:**

Popover can toggle `arrow` prop to show/hide arrow, custom animation with toggle `minimal` prop.

```html run
<template>
  <div>
    <ml-popover :arrow="false" placement="right-start">
      <template>
        No Arrow
      </template>
      <template slot="target">
        <ml-button intent="primary">No arrow</ml-button>
      </template>
    </ml-popover>
    <ml-popover minimal placement="bottom" style="margin-left: 10px;">
      <template>
        Minimal animation
      </template>
      <template slot="target">
        <ml-button intent="primary">Minimal animation</ml-button>
      </template>
    </ml-popover>
    <ml-popover :arrow="false" minimal placement="bottom" style="margin-left: 10px;">
      <template>
        No arrow and minimal animation
      </template>
      <template slot="target">
        <ml-button intent="primary">No arrow and minimal animation</ml-button>
      </template>
    </ml-popover>
  </div>
</template>
```


Popover interact type: `hover`, `click`, default: `click`.

`hover-delay` and `hover-leave-delay` to custom hover trigger delay.

```html run
<template>
  <div>
    <ml-popover interact="hover" placement="bottom">
      <template>
        Hover
      </template>
      <template slot="target">
        <ml-button intent="primary">Trigger via: Hover</ml-button>
      </template>
    </ml-popover>
    <ml-popover style="margin-left: 10px;" interact="hover" :hover-delay="3000" placement="bottom">
      <template>
        Loooooong Deley Hover
      </template>
      <template slot="target">
        <ml-button intent="primary">Loooooong Deley Show</ml-button>
      </template>
    </ml-popover>
    <ml-popover style="margin-left: 10px;" interact="hover" :hover-leave-delay="3000" placement="bottom">
      <template>
        Loooooong Deley Hover Leave
      </template>
      <template slot="target">
        <ml-button intent="primary">Loooooong Deley Hide</ml-button>
      </template>
    </ml-popover>
  </div>
</template>
```

Custom Popover Content: 
```html run
<template>
  <div>
    <ml-popover thin placement="bottom">
      <template>
        Thin means content without padding.
      </template>
      <template slot="target">
        <ml-button intent="primary">Prop `thin`</ml-button>
      </template>
    </ml-popover>
    <ml-popover style="margin-left: 10px;" thin minimal placement="bottom-start">
      <ml-menu no-shadow>
        <ml-menu-divider title="Edit"></ml-menu-divider>
        <ml-menu-item text="Cut" icon="cut" label="⌘X"></ml-menu-item>
        <ml-menu-item text="Copy" icon="duplicate" label="⌘C"></ml-menu-item>
        <ml-menu-item text="Paste" icon="clipboard" label="⌘V"></ml-menu-item>
        <ml-menu-divider title="Text"></ml-menu-divider>
        <ml-menu-item icon="align-left" text="Alignment">
          <template slot="children">
            <ml-menu-item icon="align-left" text="Left"></ml-menu-item>
            <ml-menu-item icon="align-center" text="Center"></ml-menu-item>
            <ml-menu-item icon="align-right" text="Right"></ml-menu-item>
            <ml-menu-item icon="align-justify" text="Justify"></ml-menu-item>
          </template>
        </ml-menu-item>
        <ml-menu-item icon="style" text="Style">
          <template slot="children">
            <ml-menu-item icon="bold" text="Bold" />
            <ml-menu-item icon="italic" text="Italic" />
            <ml-menu-item icon="underline" text="Underline" />
          </template>
        </ml-menu-item>
        <ml-menu-item icon="asterisk" text="Miscellaneous">
          <template slot="children">
            <ml-menu-item icon="badge" text="Badge" />
            <ml-menu-item icon="book" text="Long items will truncate when they reach max-width" />
            <ml-menu-item icon="more" text="Look in here for even more items">
              <template slot="children">
                <ml-menu-item icon="briefcase" text="Briefcase" />
                <ml-menu-item icon="calculator" text="Calculator" />
                <ml-menu-item icon="dollar" text="Dollar" />
                <ml-menu-item icon="dot" text="Shapes">
                  <template slot="children">
                    <ml-menu-item icon="full-circle" text="Full circle" />
                    <ml-menu-item icon="heart" text="Heart" />
                    <ml-menu-item icon="ring" text="Ring" />
                    <ml-menu-item icon="square" text="Square" />
                  </template>
                </ml-menu-item>
              </template>
            </ml-menu-item>
          </template>
        </ml-menu-item>
      </ml-menu>
      <template slot="target">
        <ml-button intent="primary" icon="caret-down" append>Dropdown(?) Menu</ml-button>
      </template>
    </ml-popover>
  </div>
</template>
```