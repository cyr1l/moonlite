# Avatar

Avatar component

### Avartar with `src` props

```html run
<template>
  <div>
    <ml-avatar url="http://cyrilis.com/images/upload/1394299145722.jpg"></ml-avatar>
    <ml-avatar url="http://cyrilis.com/images/upload/1394299145722.jpg" round></ml-avatar>
    <ml-avatar url="http://cyrilis.com/images/upload/1394299145722.jpg" circle></ml-avatar>
    <ml-avatar url="/logo.svg"></ml-avatar>
  </div>
</template>
```

### Avatar with `size` props:
```html run
<template>
  <div>
    <ml-avatar text="100" :size="100"></ml-avatar>
    <ml-avatar text="75" :size="75"></ml-avatar>
    <ml-avatar text="50" :size="50"></ml-avatar>
    <ml-avatar text="25" :size="25"></ml-avatar>
  </div>
</template>
```


### Avatar with `text` props:
```html run
<template>
  <div>
    <ml-avatar text="ML"></ml-avatar>
    <ml-avatar text="ML" round></ml-avatar>
    <ml-avatar text="ML" circle></ml-avatar>
    <ml-avatar text="张三" intent="primary"></ml-avatar>
    <ml-avatar text="张三" intent="warning"></ml-avatar>
    <ml-avatar text="张三" intent="danger"></ml-avatar>
  </div>
</template>
```


### Avatar with different intent color.
```html run
<template>
  <div>
    <ml-avatar text="ML" :size="75" intent="primary" round></ml-avatar>
    <ml-avatar text="ML" :size="75" intent="warning" round></ml-avatar>
    <ml-avatar text="ML" :size="75" intent="success" round></ml-avatar>
    <ml-avatar text="ML" :size="75" intent="danger" round></ml-avatar>
    <ml-avatar text="ML" :size="75" intent="info" round></ml-avatar>
    <ml-avatar text="ML" :size="75" intent="dark" round></ml-avatar>
    <ml-avatar text="ML" :size="75" intent="default" round></ml-avatar>
  </div>
</template>
```

### Avatar with different `shape` and `size`:
```html run
<template>
  <div>
    <ml-avatar></ml-avatar>
    <ml-avatar :size="75"></ml-avatar>
    <ml-avatar :size="50"></ml-avatar>
    <ml-avatar :size="25"></ml-avatar>
    <ml-avatar round></ml-avatar>
    <ml-avatar round :size="75"></ml-avatar>
    <ml-avatar round :size="50"></ml-avatar>
    <ml-avatar round :size="25"></ml-avatar>
    <ml-avatar circle></ml-avatar>
    <ml-avatar circle :size="75"></ml-avatar>
    <ml-avatar circle :size="50"></ml-avatar>
    <ml-avatar circle :size="25"></ml-avatar>
  </div>
</template>
```

### Avatar with different `icon` and `size`.
```html run
<template>
  <div>
    <ml-avatar icon="person" intent="primary"></ml-avatar>
    <ml-avatar icon="person" intent="warning" :size="75"></ml-avatar>
    <ml-avatar icon="person" :size="50"></ml-avatar>
    <ml-avatar icon="person" :size="25"></ml-avatar>
    <ml-avatar icon="person" round></ml-avatar>
    <ml-avatar icon="person" round :size="75"></ml-avatar>
    <ml-avatar icon="person" round :size="50"></ml-avatar>
    <ml-avatar icon="person" round :size="25"></ml-avatar>
    <ml-avatar icon="person" circle></ml-avatar>
    <ml-avatar icon="person" circle :size="75"></ml-avatar>
    <ml-avatar icon="person" circle :size="50"></ml-avatar>
    <ml-avatar icon="person" circle :size="25"></ml-avatar>
  </div>
</template>
```

