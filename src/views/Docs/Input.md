# Inputs

Different Size Input:

```html run
<template>
  <div>
    <ml-row :gutter="10">
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input size="xlarge" placeholder="Placeholder..."></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input size="large" placeholder="Placeholder..."></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..."></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input size="small" placeholder="Placeholder..."></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input size="mini" placeholder="Placeholder..."></ml-input>
      </ml-col>
    </ml-row>
  </div>
</template>
```


Different Input Type:

```html run
<template>
  <div>
    <ml-row :gutter="10">
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input type="text" placeholder="Text"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input type="password" placeholder="Password"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input type="number" :cols="6" placeholder="Number"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input type="email" :cols="6" placeholder="Email"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input type="textarea" :cols="6" placeholder="Textarea"></ml-input>
      </ml-col>
    </ml-row>
    <hr class="ml-hr">
    <p>禁用状态: </p>
    <ml-input disabled type="text" placeholder="Text"></ml-input>
  </div>
</template>
```

Different Intent: 

```html run
<template>
  <div>
    <ml-row :gutter="10">
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." state="primary" value="Primary"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." state="success" value="Success"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." state="danger" value="Danger"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." state="warning" value="Warning"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." state="info" value="Information"></ml-input>
      </ml-col>
    </ml-row>
  </div>
</template>
```


Input with Icons:
```html run
<template>
  <div>
    <ml-row gutter="10">
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input fill icon="envelope" placeholder="Email address here."></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input fill append icon="key" placeholder="Please input password"></ml-input>
      </ml-col>
      <ml-col>
        <hr class="ml-hr">
      </ml-col>
      <ml-col width="260px">
        <ml-input fill append @icon="toggleEye" value="hide-me-please" :icon="password ? 'eye-off' : 'eye-open'" :type="password ? 'password' : 'text'" placeholder="Please input password"></ml-input>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    return {
      password: true
    }
  },
  methods: {
    toggleEye () {
      this.password = !this.password
    }
  },
}
</script>
```

Underline Style Input:
```html run
<template>
  <div>
    <ml-row :gutter="10">
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." underline value="Primary"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." underline state="primary" value="Primary"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." underline state="success" value="Success"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." underline state="danger" value="Danger"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." underline state="warning" value="Warning"></ml-input>
      </ml-col>
      <ml-col width="260px" style="margin-bottom: 15px;">
        <ml-input placeholder="Placeholder..." underline state="info" value="Information"></ml-input>
      </ml-col>
    </ml-row>
  </div>
</template>
```

Select Raw component
```html run
<template>
  <div>
    <div class="ml-select">
      <select style="width: 200px" name="" placeholder="请选择一个选项" id="">
        <option value="0">选项 零</option>
        <option value="1" selected>选项 一</option>
        <option value="2">选项 二</option>
        <option value="3">选项 三</option>
        <option value="4">选项 四</option>
      </select>
    </div>
    <ml-select-raw>
      <option value="1">test</option>
    </ml-select-raw>
    <ml-select-raw value="3">
      <option value="0">选项 零</option>
      <option value="1" selected>选项 一</option>
      <option value="2">选项 二</option>
      <option value="3">选项 三</option>
      <option value="4">选项 四</option>
    </ml-select-raw>
  </div>
</template>


```html run
<template>
  <div>
    <ml-input type="number" max="5" min="1" v-model="data"/>
  </div>
</template>

<script>
export default {
  data () {
    return {
      data: 3
    }
  },
  watch: {
    data (newVal, oldVal) {
      if (newVal > 5 || newVal <  0) {
        this.$nextTick(() => {
          this.data = oldVal
        })
      }
    }
  }
}
</script>
```

