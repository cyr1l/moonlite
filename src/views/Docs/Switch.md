# Switch

```html run
<template>
  <div>
    <ml-switch v-model="on"></ml-switch>
    <hr class="ml-hr">
    <p>With On Text and Off Text:</p>
    <ml-switch v-model="autoUpdate" intent="primary" on-text="Auto Update" off-text="Manual Update"></ml-switch>
  </div>
</template>
<script>
export default {
  data () {
    return {
      autoUpdate: true,
      on: true
    }
  }
}
</script>
```
Different Intents
```html run
<template>
  <div>
    <ml-switch v-model="on" intent="primary" on-text="Enable" off-text="Disable"></ml-switch>
    <ml-switch v-model="on" intent="success" on-text="Enable" off-text="Disable"></ml-switch>
    <ml-switch v-model="on" intent="warning" on-text="Enable" off-text="Disable"></ml-switch>
    <ml-switch v-model="on" intent="danger" on-text="Enable" off-text="Disable"></ml-switch>
    <ml-switch v-model="on" intent="information" on-text="Enable" off-text="Disable"></ml-switch>
    <ml-switch v-model="on" intent="dark" on-text="Enable" off-text="Disable"></ml-switch>
  </div>
</template>
<script>
export default {
  data () {
    return {
      autoUpdate: true,
      on: true
    }
  }
}
</script>
```