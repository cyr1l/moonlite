# Progress

Progress component is used to display progress

```html run
<template>
  <div>
    <ml-progress :value="progress"></ml-progress>
    <hr class="ml-hr"/>
    <ml-progress indeterminate></ml-progress>
    <ml-progress indeterminate intent="primary"></ml-progress>
    <ml-information style="background: #646b7e" type="danger" :closable="false">
      <ml-progress background="#f70a4f" indeterminate background-inner="white"></ml-progress>
    </ml-information>
    <hr class="ml-hr"/>
    <ml-row>
      <ml-col :span="2">
        <ml-progress type="circle"></ml-progress>
      </ml-col>
      <ml-col :span="2">
        <ml-progress type="circle" intent="success"></ml-progress>
      </ml-col>
      <ml-col :span="2">
        <ml-progress type="circle" intent="warning"></ml-progress>
      </ml-col>
      <ml-col :span="2">
        <ml-progress type="circle" intent="danger"></ml-progress>
      </ml-col>
      <ml-col :span="2">
        <ml-progress type="circle" intent="information"></ml-progress>
      </ml-col>
      <ml-col :span="2">
        <ml-progress type="circle" intent="dark"></ml-progress>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    return {
      progress: 0.7
    }
  },
  mounted () {
    window.setInterval(() => {
      this.progress = Math.random()
    }, 1000)
  }
}
</script>
```