```html run
<template>
  <div>
    <ml-slider v-model="value"></ml-slider>
    <ml-slider :tooltip="false" v-model="value"></ml-slider>
    {{value}}
    <ml-slider v-model="values"></ml-slider>
    {{values}}
    <ml-slider :merge="false" v-model="values"></ml-slider>
    Readonly
    <ml-slider readonly v-model="values"></ml-slider>
  </div>
</template>
<script>
export default {
  data () {
    return {
      value: 40,
      values: [30, 40]
    }
  }
}
</script>
```

```html run
<template>
  <div>
    <ml-slider 
      devider=" - " 
      :tooltip="slideTooltip()" 
      :connect="[false, true, false]" 
      :range="slideRange" 
      :max="10240" 
      :value="value"
      @input="updateValue" 
    ></ml-slider>
    <p>
      Slider is based on <a href="https://refreshless.com/nouislider/">noUiSlider</a>,
      <br/>
      you can check out <code>range</code> options at: <a href="https://refreshless.com/nouislider/slider-values/">https://refreshless.com/nouislider/slider-values/</a>
    </p>
  </div>
</template>
<script>
export default {
  data () {
    return {
      value: [1024, 1024 * 5],
      slideRange: {
        'min': [512, 512],
        '10%': [1024, 1024],
        'max': 16384
      }
    }
  },
  methods: {
    slideTooltip () {
      const format = (v) => {
        const text = (parseInt(v) / 1024).toFixed(1)
        return text + ' G'
      }
      return [{ to: format }, { to: format }]
    },
    updateValue ([a, b]) {
      console.log('updateValue: ', a, b)
      this.value = [a, b]
    },
  }
}
</script>
```


```html run
<template>
  <div>
    <ml-slider 
      :tooltip="tooltipFormat" 
      :connect="[false, true, false, false]" 
      :max="10240" 
      v-model="value"
    ></ml-slider>
    <ml-slider 
      :tooltip="multipleTooltipFormatter" 
      :connect="[false, true, false, false]" 
      :max="10240" 
      v-model="value"
    ></ml-slider>
  </div>
</template>
<script>
export default {
  data () {
    return {
      value: [1024, 1024 * 5, 1025 * 8],
    }
  },
  methods: {
    tooltipFormat (v) {
      const text = (parseInt(v) / 1024).toFixed(1)
      return text + ' Gb/s'
    }
  },
  computed: {
    multipleTooltipFormatter () {
      return [true, false, this.tooltipFormat]
    }
  }
}
</script>
```

### Slider with Scale 
```html run
<template>
  <div>
    <ml-slider 
      :tooltip="tooltipFormat" 
      :max="10240" 
      :scale="scaleOption"
      v-model="value"
    ></ml-slider>
  </div>
</template>
<script>
export default {
  data () {
    return {
      value: [1024],
      scaleOption: {
        mode: 'positions',
        values: [0, 25, 50, 75, 100],
        density: 3,
        stepped: true,
        format: {
          to: this.tooltipFormat
        }
      }
    }
  },
  methods: {
    tooltipFormat (v) {
      const text = (parseInt(v) / 1024).toFixed(1)
      return text + ' Gb/s'
    }
  }
}
</script>
```


### Vertical Slider 
```html run
<template>
  <div>
    <ml-slider 
      orientation="vertical"
      :tooltip="tooltipFormat" 
      :max="10240" 
      :scale="scaleOption"
      v-model="value"
    ></ml-slider>
  </div>
</template>
<script>
export default {
  data () {
    return {
      value: [1024],
      scaleOption: {
        mode: 'positions',
        values: [0, 25, 50, 75, 100],
        density: 3,
        stepped: true,
        format: {
          to: this.tooltipFormat
        }
      }
    }
  },
  methods: {
    tooltipFormat (v) {
      const text = (parseInt(v) / 1024).toFixed(1)
      return text + ' Gb/s'
    }
  }
}
</script>
```