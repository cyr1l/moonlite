# Tag

```html run
<template>
  <div>
    <ml-tag closable>
      Tag One
    </ml-tag>
    <ml-tag>
      Tag Two
    </ml-tag>
    <ml-tag circle>
      Tag Three
    </ml-tag>
    <ml-tag circle closable @close="onClose">
      Closeable Tag
    </ml-tag>
    <ml-tag circle closable intent="primary" @close="onClose">
      Primary Tag
    </ml-tag>
    <ml-tag circle closable intent="warning" @close="onClose">
      Warning Tag
    </ml-tag>
    <ml-tag circle closable intent="danger" @close="onClose">
      Danger Tag
    </ml-tag>
    <ml-tag circle plain closable intent="danger" @close="onClose">
      Plain Danger Tag
    </ml-tag>
    <ml-tag circle plain closable @close="onClose">
      Plain Tag
    </ml-tag>
    <hr class="ml-hr">
    <p>With icon:</p>
    <ml-tag icon="tag" closable>
      Tag One
    </ml-tag>
    <ml-tag icon="tag">
      Tag Two
    </ml-tag>
    <ml-tag icon="tag" circle>
      Tag Three
    </ml-tag>
    <ml-tag icon="tag" circle closable @close="onClose">
      Closeable Tag
    </ml-tag>
    <ml-tag icon="tag" circle closable intent="primary" @close="onClose">
      Primary Tag
    </ml-tag>
    <ml-tag icon="tag" circle closable intent="warning" @close="onClose">
      Warning Tag
    </ml-tag>
    <ml-tag icon="tag" circle closable intent="danger" @close="onClose">
      Danger Tag
    </ml-tag>
    <ml-tag icon="tag" circle plain closable intent="danger" @close="onClose">
      Plain Danger Tag
    </ml-tag>
    <ml-tag icon="tag" circle plain closable @close="onClose">
      Plain Tag
    </ml-tag>
  </div>
</template>

<script>
export default {
  methods: {
    onClose () {
      this.$alert('Delete!')
    }
  }
}
</script>
```