```html run
<template>
  <div>
    <ml-checkbox name="auto-save" v-model="autoSave">
      自动保存
    </ml-checkbox>
    <ml-checkbox name="remember-me" v-model="rememberMe">
      记住密码
    </ml-checkbox>
    <ml-checkbox name="notify-others" v-model="notifyOthers">
      通知其他人
    </ml-checkbox>
    <hr class="ml-hr">
    <ml-checkbox circle name="auto-save" v-model="autoSave">
      自动保存
    </ml-checkbox>
    <ml-checkbox circle name="remember-me" v-model="rememberMe">
      记住密码
    </ml-checkbox>
    <ml-checkbox circle name="notify-others" v-model="notifyOthers">
      通知其他人
    </ml-checkbox>
    <hr class="ml-hr">
    <ml-checkbox :intent="intent" name="auto-save" v-model="autoSave" v-for="intent in styles" :key="intent">
      自动保存
    </ml-checkbox>
    <hr class="ml-hr">
    <ml-checkbox :value="true" disabled>
      已禁用
    </ml-checkbox>
    <ml-checkbox disabled>
      已禁用
    </ml-checkbox>
    <hr class="ml-hr">
    <ml-checkbox style="font-size: 20px;">
      字体大小: font-size: 20px;
    </ml-checkbox>
    <ml-checkbox circle style="font-size: 20px;">
      字体大小: font-size: 20px;
    </ml-checkbox>
  </div>
</template>

<script>
export default {
  data () {
    return {
      autoSave: '',
      rememberMe: '',
      notifyOthers: '',
      styles: [
        'default', 'dark', 'primary', 'danger', 'warning', 'success', 'info'
      ]
    }
  }
}
</script>
```
