# Button

> Buttons trigger actions when clicked.


## Simple Usage

### Different Intent(Color)
```html run
<template>
  <div>
    <ml-button>
      Default Button
    </ml-button>
    <ml-button intent="dark">
      Dark Button
    </ml-button>
    <ml-button intent="gray">
      Gray Button
    </ml-button>
    <hr class="ml-hr">
    <ml-button intent="primary">
      Primary Button
    </ml-button>
    <ml-button intent="success">
      Success Button
    </ml-button>
    <ml-button intent="danger">
      Danger Button
    </ml-button>
    <ml-button intent="warning">
      Warning Button
    </ml-button>
    <ml-button intent="info">
      Information Button
    </ml-button>
    <ml-button intent="info" icon="grid">
      中文按钮测试
    </ml-button>
  </div>
</template>
```

### Different Type
```html run
<template>
  <div>
    <ml-button intent="primary" type="default">
      Default Type
    </ml-button>
    <ml-button intent="primary" type="hollow">
      Hollow Button
    </ml-button>
    <ml-button intent="primary" type="subtle">
      Subtle Button  
    </ml-button>
    <ml-button intent="primary" type="link">
      Link Button  
    </ml-button>
  </div>
</template>
```

### Mix Button Type and Intent
```html run
<template>
  <div>
    <ml-button type="default" intent="success">
      Success Button
    </ml-button>
    <ml-button type="hollow" intent="dark">
      Dark Hollow
    </ml-button>
    <ml-button type="subtle" intent="primary">
      Primary Subtle
    </ml-button>
    <ml-button type="subtle" intent="danger">
      Danger Subtle
    </ml-button>
    <ml-button type="link" intent="warning">
      Warning Link
    </ml-button>
  </div>
</template>
```

### Prepend or Append  Button Icon
```html run
<template>
  <div>
    <div class="align-center margin-bottom double">
      <router-link :to="{name: 'Icons'}">
        <ml-button icon="link" type="link">
          View all avaliable icons
        </ml-button>
      </router-link>
    </div>
    <ml-button icon="export">Export</ml-button>
    <ml-button icon="search" intent="dark">Search</ml-button>
    <ml-button intent="gray" icon="locate">Locate</ml-button>
    <ml-button intent="primary" icon="plus">Plus</ml-button>
    <ml-button intent="success" icon="tick">Confirm</ml-button>
    <ml-button intent="danger" icon="trash">Delete</ml-button>
    <ml-button intent="warning" icon="refresh">Reload</ml-button>
  </div>
</template>
```

### Differenct Button Size:
```html run
<template>
  <div>
    <ml-button size="xlarge" intent="primary">
      XLarge Button
    </ml-button>
    <ml-button size="large" intent="primary">
      Large Button
    </ml-button>
    <ml-button size="default" intent="primary">
      Default Button
    </ml-button>
    <ml-button size="small" intent="primary">
      Small Button
    </ml-button>
    <ml-button size="mini" intent="primary">
      Mini Button
    </ml-button>
  </div>
</template>
```

### Other Props: (`circle`, `round`, `square`, `disabled`, `fill`)
```html run
<template>
  <div>
    <h3>Circle</h3>
    <ml-button intent="primary" icon="download">
      Normal Button
    </ml-button>
    <ml-button intent="primary" icon="download" circle>
      Circle Button
    </ml-button>
    <ml-button intent="danger" icon="big-bang" circle>
      Circle Button
    </ml-button>
    <ml-button intent="success" icon="tick" circle>
      Circle Button
    </ml-button>
    <ml-button intent="primary" icon="download" type="hollow" circle>
      Circle Button
    </ml-button>
    <h3>Square</h3>
    <ml-button intent="primary" icon="download">
      Normal Button
    </ml-button>
    <ml-button intent="primary" icon="download" square>
      Square Button
    </ml-button>
    <ml-button intent="primary" icon="download" type="hollow" square>
      Square Button
    </ml-button>
    <h3>Round</h3>
    <ml-button intent="primary" icon="download">
      Normal Button
    </ml-button>
    <ml-button intent="primary" icon="download" round>
      Round Button
    </ml-button>
    <ml-button intent="primary" icon="download" type="hollow" round>
      Round Hollow Button
    </ml-button>
    <h3>Disabled</h3>
    <ml-button intent="primary" icon="download">
      Normal Button
    </ml-button>
    <ml-button intent="primary" icon="download" disabled>
      Disabled Button
    </ml-button>
    <ml-button intent="primary" icon="download" type="hollow" disabled>
      Disabled Hollow Button
    </ml-button>
    <h3>Fill</h3>
    <ml-button intent="primary" icon="download">
      Normal Button
    </ml-button>
    <ml-button intent="primary" icon="download" fill>
      Fill Button
    </ml-button>
    <ml-button intent="primary" icon="download" type="hollow" fill>
      Fill Hollow Button
    </ml-button>
  </div>
</template>
```
