# Select2


## Simple Usage
```html run
<template>
  <div>
    <ml-select2 placeholder="Please choose an option..." :template="template">
      <option value="1">
        Hello world
      </option>
      <option value="2">
        Hello MoonLite
      </option>
      <option value="3">
        Hello Something else.
      </option>
    </ml-select2>
    <ml-select2 placeholder="Please choose an option..." :template="template" search :data="data"></ml-select2>
    <ml-select2 disabled placeholder="Please choose an option..." :template="template" :data="data"></ml-select2>
    <hr class="ml-hr"/>
    <div>
      <ml-select2 search multiple style="width: 400px" placeholder="请选择城市">
        <optgroup label="Alaskan/Hawaiian Time Zone">
          <option value="AK">Alaska</option>
          <option value="HI">Hawaii</option>
        </optgroup>
        <optgroup label="Pacific Time Zone">
          <option value="CA">California</option>
          <option value="NV">Nevada</option>
          <option value="OR">Oregon</option>
          <option value="WA">Washington</option>
        </optgroup>
      </ml-select2>
    </div>
  </div>
</template>

<script>
export default {
  data () {
    return {
      data: [
        {
          text: 'Hello world',
          id: 1
        },
        {
          text: 'Hello MoonLite',
          id: 2
        },
        {
          text: 'Hello Something Else',
          id: 3
        }
      ],
      template: `#{{id}} {{text}}`,
    }
  }
}
</script>
```

## Select Ajax Remote Data:

Ajax Configuration from: 

https://select2.org/data-sources/ajax

```html run
<template>
  <div>
    <ml-row>
      <ml-col>
        <ml-select2 @input="select2Change" @select="select2Select" style="width: 400px;" min-search="2" placeholder="查询 GitHub Repo..." :template="repoTemplate" search :ajax="ajax"></ml-select2>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    return {
      ajax: {
        url: 'https://api.github.com/search/repositories',
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term,
            page: params.page
          }
        },
        processResults: function (data, params) {
          params.page = params.page || 1
          return {
            results: data.items,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
          }
        },
        cache: true
      },
      repoTemplate: `{{full_name}}`
    }
  },
  methods: {
    select2Change (value) {
      console.log('select2Change: ', value)
    },
    select2Select (selection) {
      console.log('Select2Select: ', selection)
    }
  }
}
</script>
```

## Select Options via Promise

param: `fetch`: 

```js
let fetchFunction = function (query, page) {
  return new Promise((resolve, reject) => {
    let {data, has_next} = await request(url)
    if (data) {
      resolve({
        result: data,
        pagination: {
          more: has_next
        } 
      })
    } else {
      reject('Not Found')
    }
  })
}
```
```html run
<template>
  <div>
    <ml-row>
      <ml-col>
        <ml-select2 style="width: 400px;" min-search="1" placeholder="Promise Options..." search :fetch="fetch"></ml-select2>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    return {
      fetch: function (query, page) {
        return Promise.resolve({
          results: [{
            id: '1',
            text: 'Hello'
          }, {
            id: '2',
            text: 'world'
          }],
          pagination: {
            more: true
          }
        })
      }
    }
  }
}
</script>
```
