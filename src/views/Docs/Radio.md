```html run
<template>
  <div>
    <ml-radio-group name="option1" v-model="option1">
      <ml-radio value="optionA">
        选项 A
      </ml-radio>
      <ml-radio value="optionB">
        选项 B
      </ml-radio>
      <ml-radio value="optionC">
        选项 C
      </ml-radio>
      <ml-radio value="optionD">
        选项 D
      </ml-radio>
      <ml-radio value="optionD">
        选项 E
      </ml-radio>
    </ml-radio-group>
    <hr class="ml-hr">
    <ml-radio square name="auto-save" value="yes" v-model="autoSave">
      自动保存
    </ml-radio>
    <ml-radio square name="auto-save" :value="'no'" v-model="autoSave">
      自动保存
    </ml-radio>
    <ml-radio square name="remember-me" value="remember" v-model="rememberMe">
      记住密码
    </ml-radio>
    <ml-radio square name="notify-others" value="notify" v-model="notifyOthers">
      通知其他人
    </ml-radio>
    <hr class="ml-hr">
    <ml-radio :intent="intent" :value="intent" name="auto-save" v-model="autoSave" v-for="intent in styles" :key="intent">
      自动保存
    </ml-radio>
    <hr class="ml-hr">
    <ml-radio :value="true" disabled>
      已禁用
    </ml-radio>
    <ml-radio disabled>
      已禁用
    </ml-radio>
    <hr class="ml-hr">
    <ml-radio style="font-size: 20px;">
      字体大小: font-size: 20px;
    </ml-radio>
    <ml-radio square style="font-size: 20px;">
      字体大小: font-size: 20px;
    </ml-radio>
  </div>
</template>

<script>
export default {
  data () {
    return {
      option1: 'optionA',
      autoSave: 'yes',
      notifyOthers: false,
      rememberMe: false,
      styles: [
        'default', 'dark', 'primary', 'danger', 'warning', 'success', 'info'
      ]
    }
  }
}
</script>
```