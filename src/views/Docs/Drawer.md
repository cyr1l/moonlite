# Drawer 抽屉

## 基本用法

呼出一个临时的侧边栏, 可以从多个方向呼出。

```html run
<template>
  <div>
    <ml-radio-group name="placement" v-model="placement">
      <ml-radio value="top">
        上边
      </ml-radio>
      <ml-radio value="bottom">
        下边
      </ml-radio>
      <ml-radio value="left">
        左边
      </ml-radio>
      <ml-radio value="right">
        右边
      </ml-radio>
    </ml-radio-group>
    <br />
    <ml-button intent="primary" @click="drawerVisible = true">
      点我打开
    </ml-button>

    <div style="text-align: left;">
      <ml-drawer
        :visible.sync="drawerVisible"
        title="标题"
        :placement="placement"
      >
      </ml-drawer>
    </div>
  </div>
</template>
<script>
  export default {
    data() {
      return {
        drawerVisible: false,
        placement: "right",
      };
    },
  };
</script>
```

## 自定 title 样式

可以使用 slot 调整顶部标题样式

```html run
<template>
  <div>
    <ml-button intent="primary" @click="drawerRightVisible = true">
      自定义标题
    </ml-button>

    <div style="text-align: left;">
      <ml-drawer :visible.sync="drawerRightVisible" title="标题">
        <template v-slot:title>
          <h1 style="padding: 14px;margin: 0;">
            自定义标题
          </h1>
        </template>
        <div>正文内容</div>
      </ml-drawer>
    </div>
  </div>
</template>
<script>
  export default {
    data() {
      return {
        drawerRightVisible: false,
      };
    },
  };
</script>
```

## 嵌套 Drawer

嵌套 Drawer 最好按照顺序同级书写

```html run
<template>
  <div>
    <ml-button intent="primary" @click="drawerVisible1 = true">
      点击第一层Drawer
    </ml-button>

    <div style="text-align: left;">
      <ml-drawer :visible.sync="drawerVisible1" title="第一层Drawer">
        <ml-button intent="primary" @click="drawerVisible2 = true">
          点击第二层Drawer
        </ml-button>
      </ml-drawer>
      <ml-drawer
        :visible.sync="drawerVisible2"
        title="第二层Drawer"
        width="300px"
      >
        <ml-button intent="primary" @click="drawerVisible3 = true">
          点击第三层Drawer
        </ml-button>
      </ml-drawer>
      <ml-drawer
        :visible.sync="drawerVisible3"
        title="第三层Drawer"
        placement="top"
      ></ml-drawer>
    </div>
  </div>
</template>
<script>
  export default {
    data() {
      return {
        drawerVisible1: false,
        drawerVisible2: false,
        drawerVisible3: false,
      };
    },
  };
</script>
```

## 延时关闭抽屉

before-close 可以在关闭抽屉前处理任务

```html run
<template>
  <div>
    <ml-button intent="primary" @click="drawerVisible = true">
      点我弹出抽屉
    </ml-button>

    <ml-drawer
      style="text-align: left;"
      :visible.sync="drawerVisible"
      title="延时2s关闭"
      :closable="true"
      :before-close="beforeHandle"
      @close="closedHandle"
    ></ml-drawer>
  </div>
</template>
<script>
  export default {
    data() {
      return {
        drawerVisible: false,
      };
    },
    methods: {
      beforeHandle() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve();
          }, 2000);
        });
      },
      closedHandle() {
        this.$message({
          type: "success",
          message: "关闭成功！",
        });
      },
    },
  };
</script>
```

## API

### Drawer Attributes

| 参数          | 说明                                                              | 类型     | 默认值 |
| :------------ | :---------------------------------------------------------------- | -------- | ------ |
| visible       | 是否显示 Drawer，支持 .sync 修饰符                                | Boolean  | false  |
| title         | Drawer 的标题，也可通过具名 slot 传入。如果传入为空，则不显示标题 | String   | -      |
| placement     | Drawer 放置的方向 （可选值为 top / bottom / left / right ）       | String   | right  |
| width         | Drawer 的宽度(placement 为左右方向时生效)                         | String   | 500px  |
| height        | Drawer 的宽度(placement 为上下方向时生效)                         | String   | 300px  |
| mask-closable | 是否允许点击遮罩层关闭                                            | Boolean  | true   |
| mask          | 是否显示遮罩层                                                    | Boolean  | true   |
| closable      | 是否显示右上角的关闭按钮                                          | Boolean  | false  |
| before-close  | 关闭前的回调，返回 Promise 可以阻止关闭                           | function | -      |

<br>

### Drawer Events

| 事件名称 | 说明           |
| :------- | :------------- |
| close    | 关闭抽屉时触发 |

<br>

### Drawer slot

| 名称  | 说明                |
| :---- | :------------------ |
| title | Drawer 标题区的内容 |
