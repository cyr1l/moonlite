# Information 

### Basic usage
```html run
<template>
  <div>
    <ml-information title="Information title here" message="Alert components are non-overlay elements in the page that does not disappear automatically."/>
    <ml-information type="primary" title="Information title here" message="Alert components are non-overlay elements in the page that does not disappear automatically."/>
    <ml-information type="primary" :closable="false" title="Information title here" message="Alert components are non-overlay elements in the page that does not disappear automatically."/>
    <ml-information icon="warning-sign" type="primary" title="Information title here" message="Alert components are non-overlay elements in the page that does not disappear automatically."/>
    <ml-information icon="warning-sign" heavy type="primary" title="Information title here" message="Alert components are non-overlay elements in the page that does not disappear automatically."/>
  </div>
</template>
```

### More intent style
```html run
<template>
  <div>
    <ml-radio :intent="style" :value="style" name="auto-save" v-model="intent" v-for="style in styles" :key="style" >
      <span style="padding:4px 10px; border-radius: 2px" class="ml-text-intent-white" :class="'ml-text-background-' + style">
        {{style}}
      </span>
    </ml-radio>
    <br/>
    <br/>
    <ml-switch v-model="heavy" off-text="Not Heavy" on-text="Heavy"/>
    <hr class="ml-hr"/>
    <ml-information :heavy="heavy" :type="intent" title="Information title here" message="Alert components are non-overlay elements in the page that does not disappear automatically."/>
  </div>
</template>
<script>
export default  {
  data () {
    return {
      intent: 'primary',
      heavy: false,
      styles: [
        'default', 'dark', 'primary', 'danger', 'warning', 'success', 'info'
      ]
    }
  }
}
</script>
```

### Custom
```html run
<template>
  <div>
    <ml-information type="success" title="Only Title"></ml-information>
    <ml-information type="warning" message="Only Body message here."></ml-information>
    <hr class="ml-hr">
    <ml-information icon="help" type="danger" title="Custom Icon" message="Alert components are non-overlay elements in the page that does not disappear automatically."></ml-information>
    <ml-information icon="download" :closable="false" type="danger" title="Message as slot and not closable">
      Alert components are non-overlay elements in the page that does not disappear automatically.
    </ml-information>
    <ml-information >
      <div class="ml-information-title has-message">
        Custom Title and custom Body
      </div>
      <div class="ml-information-message">
        Alert components are non-overlay elements in the page that does not disappear automatically.
      </div>
    </ml-information>
    <ml-information>
      <div class="ml-information-icon">
        <ml-icon name="download"></ml-icon>
      </div>
      <div class="ml-information-body">
        <div class="ml-information-title has-message">
          Custom Title and Body and Icon
        </div>
        <div class="ml-information-message">
          Alert components are non-overlay elements in the page that does not disappear automatically.
        </div>
      </div>
    </ml-information>
  </div>
</template>

```