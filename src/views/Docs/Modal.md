# Modal

Modal (or Dialog) present content overlaid over other parts of the UI.

### Basic Usage:

```html run
<template>
  <div>
    <ml-button intent="danger" type="default" icon="trash" @click="modalVisible = !modalVisible">
      Delete this
    </ml-button>
    <ml-modal class="test-modal" width="550px" keep title="Delete Confirm" :visible.sync="modalVisible">
      <p>Are your sure to delete this item? This action cannot undo.</p>
      <template slot="footer">
        <div class="align-right">
          <ml-button type="subtle" icon="info-sign" @click="modalVisible = false">
            Cancel
          </ml-button>
          <ml-button type="danger" icon="warning-sign" @click="sendMessage('danger')">
            Sure, delete anyway.
          </ml-button>
        </div>
      </template>
    </ml-modal>
  </div>
</template>

<script>
export default {
  data () {
    return {
      modalVisible: false
    }
  },
  methods: {
    sendMessage () {
      // hide model
      this.modalVisible = false
      this.$message({
        type: "success",
        message: 'This item is deleted successfully!'
      })
    }
  }
}
</script>
```

### Custom Modal UI:

```html run
<template>
  <div>
    <ml-button intent="success" type="default" icon="plus" @click="modalVisible = !modalVisible">
      Create New Version
    </ml-button>
    <ml-modal title="Create Project Version" :visible.sync="modalVisible">
      <div v-loading="creating">
        <div class="center-line"></div>
        <div class="top-line"></div>
        <ml-row :gutter="10">
          <ml-col>
            <ml-label title="Choose Project">
              <template>
                <ml-select :data="projects" placeholder="Please choose a project" style="width: 100%">
                </ml-select>
              </template>
            </ml-label>
            <ml-label title="Version Name">
              <template slot-scope="scope">
                <ml-input :id="scope.id" fill focus></ml-input>
              </template>
            </ml-label>
            <ml-label title="Version Description">
              <template slot-scope="scope">
                <ml-input type="textarea" :rows="5" :id="scope.id" fill></ml-input>
              </template>
            </ml-label>
          </ml-col>
        </ml-row>
      </div>
      <template>
        <div class="align-right">
          <ml-button  :disabled="creating" type="success" @click="createVersion" :icon="creating ? 'loading' : 'plus'">Create Project Version</ml-button>
        </div>
      </template>
    </ml-modal>
  </div>
</template>

<script>
export default {
  data () {
    return {
      projects: [{
        text: 'Project One',
        id: 'pj-1'
      },
      {
        text: 'Project Two',
        id: 'pj-2'
      },
      {
        text: 'Project Three',
        id: 'pj-3'
      }],
      creating: false,
      modalVisible: false
    }
  },
  methods: {
    createVersion () {
      this.creating = true
      window.setTimeout(() => {
        this.modalVisible = false
        this.creating = false
        this.$message({
          type: "success",
          message: 'Version created successfully!'
        })
      }, 1000)
    }
  }
}
</script>
```

```html run
<template>
  <div>
    <ml-button intent="info" type="warning" @click="toggleModal">Show Loading Modal</ml-button>
    <ml-modal width="500px" title="Loading Modal" :visible.sync="visible" v-loading="loading">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus voluptatibus voluptates esse iure soluta nemo consectetur sapiente, incidunt necessitatibus qui nam eveniet cupiditate beatae odio! Eligendi ea similique facere officiis!
      </p>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda porro vel dicta atque rem odit ad molestias! Cum atque voluptates amet a rerum, vel veritatis, fugit molestias, sit nihil magnam.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum rem, eos ipsa consequatur totam doloribus esse, at fugiat, omnis est libero eligendi perferendis. Iusto perspiciatis accusamus quam laudantium, impedit fugit!</p>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur ut eveniet provident veritatis itaque modi saepe animi optio! Voluptas ut sed, repudiandae neque deserunt consequatur modi quos accusantium vero rem.</p>
      <template slot="footer">
        <ml-button @click="loadingToggle">Loading 3 seconds</ml-button>
      </template>
    </ml-modal>
  </div>
</template>
<script>
export default {
  data () {
    return {
      loading: true,
      visible: false
    }
  },
  methods: {
    toggleModal () {
      this.visible = true
      this.loadingToggle()
    },
    loadingToggle () {
      this.loading = true
      window.setTimeout(() => {
        this.loading = false
      }, 3000)
    }
  }
}
</script>
```


### Multiple Modals Layers

```html run
<template>
  <div>
    <ml-button intent="info" type="default" icon="applications" @click="toggleModals">
      Show Multiple Modals
    </ml-button>
    <ml-modal width="400px" title="Clone Repository" :actions="modalActions" :visible.sync="modalVisibleList[0]">
      <ml-input focus placeholder="Search repos..." fill icon="search" append></ml-input>
    </ml-modal>
    <ml-modal width="450px" title="Clone Repository" :actions="modalActions" :visible.sync="modalVisibleList[1]">
      <ml-input focus placeholder="Search repos..." fill icon="search" append></ml-input>
    </ml-modal>
    <ml-modal width="500px" title="Clone Repository" :actions="modalActions" :visible.sync="modalVisibleList[2]">
      <ml-input focus placeholder="Search repos..." fill icon="search" append></ml-input>
    </ml-modal>
  </div>
</template>

<script>
export default {
  data () {
    return {
      modalVisibleList: [
        false,
        false,
        false,
      ],
      modalActions: [
        {
          type: 'subtle',
          text: 'Cancel',
          callback: () => {
            this.$message('Cancel')
          }
        },
        {
          type: 'primary',
          text: 'Clone Repo',
          icon: 'git-push',
          callback: () => {
            this.$message('OK')
          }
        }
      ]
    }
  },
  methods: {
    toggleModals () {
      Array.from(Array(6)).forEach((v, k) => {
        window.setTimeout(() => {
          this.$set(this.modalVisibleList, k, !this.modalVisibleList[k])
        }, k * 500)
        window.setTimeout(() => {
          k = 3 - k
          this.$set(this.modalVisibleList, k, !this.modalVisibleList[k])
        }, (k + 3) * 500)
      })
    }
  }
}
</script>
```



### Confirm, Alert, Prompt

```html run
<template>
  <div>
    <ml-button intent="warning" icon="confirm" @click="showConfirmModal">
      Show Confirm Modal
    </ml-button>
    <ml-button intent="info" icon="confirm" @click="showPromptModal">
      Show Prompt Modal
    </ml-button>
  </div>
</template>

<script>
export default {
  data () {
    return {
    }
  },
  methods: {
    showConfirmModal () {
      this.$confirm({
        title: 'Hold on',
        message: 'Are you sure to delete this item?',
        keep: true,
        cancel: {
          text: 'Wait a minute...',
          type: 'subtle',
          dismiss: true
        },
        confirm: {
          type: 'danger',
          icon: 'trash',
          text: 'DELETE IT',
          dismiss: true
        }
      }).then(res => {
        this.$message('Delete ANYWAY!')
      }, error => {
        this.$message('Errrr... I changed my mind.')
      })
    },
    showPromptModal () {
      this.$prompt({
        title: 'Hello there,',
        message: 'What\'s your name?',
        keep: true,
        confirm: {
          type: 'primary',
          text: 'Send',
          dismiss: true
        },
        cancel: {
          text: 'I won\'t tell you',
          type: 'subtle',
          dismiss: true
        }
      }).then(res => {
        this.$alert('Welcome, ' + res).then(res => {
          this.$alert('You just clicked alert modal')
        })
      }, error => {
        this.$message('Welcome, Anonymous', error)
      })
    }
  }
}
</script>
```
