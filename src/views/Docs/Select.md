# Select

Use Select for choosing one item from a list. The component's children will be wrapped in a Popover that contains the list and an optional InputGroup to filter it. Provide a predicate to customize the filtering algorithm. The value of a Select (the currently chosen item) is uncontrolled: listen to changes with onItemSelect.

```html run
<template>
  <div>
    <ml-select :data="films" v-model="value" arrow animate search clearable></ml-select>
    <ml-select :data="films" v-model="value" arrow animate search></ml-select>
    <hr class="ml-hr"/>
    <ml-label horizontal title="With Arrow and Animation:">
      <ml-select :data="films" v-model="value" arrow animate></ml-select>
    </ml-label>
    <ml-label horizontal title="Without Animation:">
      <ml-select :data="films" v-model="value" arrow></ml-select>
    </ml-label>
    <ml-label horizontal title="Without Arrow:">
      <ml-select :data="films" v-model="value"></ml-select>
    </ml-label>
    <ml-label horizontal title="With Search Field">
      <ml-select :data="films" v-model="value" search></ml-select>
    </ml-label>
  </div>
</template>
<script>
var films = [
  { title: 'The Shawshank Redemption', year: 1994, disabled: true },
  { title: 'The Godfather', year: 1972 },
  { title: 'The Godfather: Part II', year: 1974 },
  { title: 'The Dark Knight', year: 2008 },
  { title: '12 Angry Men', year: 1957 },
  { title: 'Schindler\'s List', year: 1993 },
  { title: 'Pulp Fiction', year: 1994 },
  { title: 'The Lord of the Rings: The Return of the King', year: 2003 },
  { title: 'The Good, the Bad and the Ugly', year: 1966 },
  { title: 'Fight Club', year: 1999 },
  { title: 'The Lord of the Rings: The Fellowship of the Ring', year: 2001 },
  { title: 'Star Wars: Episode V - The Empire Strikes Back', year: 1980 },
  { title: 'Forrest Gump', year: 1994 },
  { title: 'Inception', year: 2010 },
  { title: 'The Lord of the Rings: The Two Towers', year: 2002 },
  { title: 'One Flew Over the Cuckoo\'s Nest', year: 1975 },
  { title: 'Goodfellas', year: 1990 },
  { title: 'The Matrix', year: 1999 }]
export default {
  data () {
    return {
      films: films.map(film => {
        film.alt = film.year
        return film
      }),
      value: { title: 'The Godfather: Part II', year: 1974 }
    }
  }
}
</script>
```

### Multiple Select Value
```html run
<template>
  <ml-row :gutter="10">
    <ml-col :span="4">
      <ml-select fill :data="films" v-model="value" search></ml-select>
    </ml-col>
    <ml-col :span="8">
      <ml-select fill :data="films" v-model="multipleValue" multiple search></ml-select>
    </ml-col>
  </ml-row>
</template>
<script>
var films = [
  { title: 'The Shawshank Redemption', year: 1994 },
  { title: 'The Godfather', year: 1972 },
  { title: 'The Godfather: Part II', year: 1974 },
  { title: 'The Dark Knight', year: 2008 },
  { title: '12 Angry Men', year: 1957 },
  { title: 'Schindler\'s List', year: 1993 },
  { title: 'Pulp Fiction', year: 1994 },
  { title: 'The Lord of the Rings: The Return of the King', year: 2003 },
  { title: 'The Good, the Bad and the Ugly', year: 1966 },
  { title: 'Fight Club', year: 1999 },
  { title: 'The Lord of the Rings: The Fellowship of the Ring', year: 2001 },
  { title: 'Star Wars: Episode V - The Empire Strikes Back', year: 1980 },
  { title: 'Forrest Gump', year: 1994 },
  { title: 'Inception', year: 2010 },
  { title: 'The Lord of the Rings: The Two Towers', year: 2002 },
  { title: 'One Flew Over the Cuckoo\'s Nest', year: 1975 },
  { title: 'Goodfellas', year: 1990 },
  { title: 'The Matrix', year: 1999 }]
export default {
  data () {
    return {
      films: films.map(film => {
        film.alt = film.year
        return film
      }),
      value: { title: 'The Godfather: Part II', year: 1974 },
      multipleValue: [
        { title: 'The Dark Knight', year: 2008 },
        { title: '12 Angry Men', year: 1957 },
        { title: 'Pulp Fiction', year: 1994 }
      ]
    }
  }
}
</script>
```



### Value Binding
Selection bind select value via `prop`.

```html run
<template>
  <ml-row :gutter="10" class="align-left">
    <ml-col :span="6">
      <ml-information :closable="false" class="ml-text-running">
        Without `prop`, v-model accept whole selection instance as value.
      </ml-information>
      <ml-select :data="films" v-model="selectionValue" search fill>
      </ml-select>
      <p>The model value without  <ml-tag>prop</ml-tag> property: </p>
      <pre class="logs"><code>{{selectionValue | json}}</code></pre>
    </ml-col>
    <ml-col :span="6">
      <ml-information :closable="false" class="ml-text-running">
        With `prop`, 
        <code>v-model</code> 
        reference to the value of the prop in the selection instance.  
      </ml-information>
      <ml-select :data="films" v-model="selectionValueProp" prop="id" search fill>
      </ml-select>
      <p>The model value with <ml-tag>prop</ml-tag> property: </p>
      <pre class="logs"><code>{{selectionValueProp | json}}</code></pre>
    </ml-col>
  </ml-row>
</template>
<script>
import uuid from 'uuid'

var films = [
  { title: 'The Shawshank Redemption', id: '26691404-f3c2-439d-9641-d5942af17df6'},
  { title: 'The Godfather', id: '3e32bbf3-4571-45fc-b2a8-e7fd4d7aafc7'},
  { title: 'The Godfather: Part II', id: 'f7b95a6c-2b27-4ce6-9158-f8a77ab01c97'},
  { title: 'The Dark Knight', id: 'ff3100e0-a761-4846-bfdf-3acd3d4086ce'},
  { title: '12 Angry Men', id: 'f0a8985e-62b4-45b0-a31e-fba12b2ab8c5'},
  { title: 'Schindler\'s List', id: 'd90b334c-c298-4a0d-bbb5-d877581f021c'},
  { title: 'Pulp Fiction', id: 'a9935941-1f6c-490a-b847-24e81e86c72b'},
  { title: 'The Lord of the Rings: The Return of the King', id: '200dcd15-3e8d-46fa-9b05-e0863014a19a'},
  { title: 'The Good, the Bad and the Ugly', id: '68d8656a-826e-4327-bb1f-abf6b60c4b3e'},
  { title: 'Fight Club', id: 'ff9f8503-fc7f-4973-b59d-2e21987b7b83'},
  { title: 'The Lord of the Rings: The Fellowship of the Ring', id: 'b6642cb0-ec1e-4b1d-858d-557bbab0efd1'},
  { title: 'Star Wars: Episode V - The Empire Strikes Back', id: 'f65fe789-648f-472e-b314-c1cc489f2492'},
  { title: 'Forrest Gump', id: 'f0aa4680-c61d-441c-8cab-59b225128903'},
  { title: 'Inception', id: 'c6f7d0c5-70e5-4fde-b679-06b4a8206590'},
  { title: 'The Lord of the Rings: The Two Towers', id: 'b97e224e-6158-4b4a-836f-3d2cebe7b4e7'},
  { title: 'One Flew Over the Cuckoo\'s Nest', id: '75c31532-e0ae-4c0e-bdd4-029291bd0035'},
  { title: 'Goodfellas', id: '78e74971-97ec-4435-af3b-dd8260f8c975'},
  { title: 'The Matrix', id: '14335635-3d19-4d20-bef3-36c4c2884d59'}
]

export default {
  data () {
    return {
      films: films,
      selectionValueProp: '',
      selectionValue: ''
    }
  },
  filters: {
    json: (value) => { return JSON.stringify(value, null, 2) }
  }
}
</script>
<style lang="stylus">
@import '~@/stylesheets/vars'
.logs {
  text-align: left;
  padding: 10px;
  background: $dark;
  color: $white;
  font-size: $fontSize;
  max-height: 300px;
  overflow: auto;
}
</style>
```

### Template
Custom Selection View.
```html run
<template>
  <div>
    <ml-select :data="films" v-model="value" search>
      <template slot="template" slot-scope="scope">
        {{scope.index}}. 
        <ml-tag intent="primary" :plain="scope.selected">
          <code>{{scope.data.year}}</code>
        </ml-tag>
        {{scope.data.title}}
      </template>
    </ml-select>
  </div>
</template>
<script>
var films = [
  { title: 'The Shawshank Redemption', year: 1994 },
  { title: 'The Godfather', year: 1972 },
  { title: 'The Godfather: Part II', year: 1974 },
  { title: 'The Dark Knight', year: 2008 },
  { title: '12 Angry Men', year: 1957 },
  { title: 'Schindler\'s List', year: 1993 },
  { title: 'Pulp Fiction', year: 1994 },
  { title: 'The Lord of the Rings: The Return of the King', year: 2003 },
  { title: 'The Good, the Bad and the Ugly', year: 1966 },
  { title: 'Fight Club', year: 1999 },
  { title: 'The Lord of the Rings: The Fellowship of the Ring', year: 2001 },
  { title: 'Star Wars: Episode V - The Empire Strikes Back', year: 1980 },
  { title: 'Forrest Gump', year: 1994 },
  { title: 'Inception', year: 2010 },
  { title: 'The Lord of the Rings: The Two Towers', year: 2002 },
  { title: 'One Flew Over the Cuckoo\'s Nest', year: 1975 },
  { title: 'Goodfellas', year: 1990 },
  { title: 'The Matrix', year: 1999 }]
export default {
  data () {
    return {
      films: films.map(film => {
        film.alt = film.year
        return film
      }),
      value: { title: 'The Godfather: Part II', year: 1974 }
    }
  }
}
</script>
```


### Playground 

```html run
<template>
  <div>
    <!-- <ml-select prop="title" disabled multiple v-model="value" :data="films"></ml-select> -->
    <hr class="ml-hr">
    <ml-select v-show="show" fill prop="title" :data="films"></ml-select>
    <hr class="ml-hr">
    <ml-select v-show="show" fill prop="id" v-model="value">
      <option value="value1">Value 1</option>
      <option value="value2">Value 2</option>
    </ml-select>
    {{value}}
  </div>
</template>

<script>
export default {
  data () {
    return {
      show: false,
      value: 'value1',
      films: []
    }
  },
  mounted () {
    window.setTimeout( () => {
      this.films = [
        { title: 'The Shawshank Redemption', year: 1994 },
        { title: 'The Godfather', year: 1972 },
        { title: 'The Godfather: Part II', year: 1974 },
        { title: 'The Dark Knight', year: 2008 },
        { title: '12 Angry Men', year: 1957 },
        { title: 'Schindler\'s List', year: 1993 },
        { title: 'Pulp Fiction', year: 1994 },
        { title: 'The Lord of the Rings: The Return of the King', year: 2003 },
        { title: 'The Good, the Bad and the Ugly', year: 1966 },
        { title: 'Fight Club', year: 1999 },
        { title: 'The Lord of the Rings: The Fellowship of the Ring', year: 2001 },
        { title: 'Star Wars: Episode V - The Empire Strikes Back', year: 1980 },
        { title: 'Forrest Gump', year: 1994 },
        { title: 'Inception', year: 2010 },
        { title: 'The Lord of the Rings: The Two Towers', year: 2002 },
        { title: 'One Flew Over the Cuckoo\'s Nest', year: 1975 },
        { title: 'Goodfellas', year: 1990 },
        { title: 'The Matrix', year: 1999 }]
      this.show = true
    }, 1000)
  }
}
</script>
```


## Select Options via Promise

param: `fetch`:

```js
let fetchFunction = function (query, page) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: 'https://api.github.com/search/repositories',
      type: 'get',
      data: { q: query, page: page}
    }).then(function (result) {
      resolve({
        more: result.total_count > (page * 30),
        data: result.items.map(d => {
          d.title = d.name
          d.alt = d.full_name
          return d
        })
      }) 
    }, function (err) {
      reject(err)
    })
  })
}
```
```html run
<template>
  <div>
    <ml-row>
      <ml-col>
        <ml-select v-model="remote" style="width: 400px;" min-search="1" prop="id" multiple placeholder="Promise Options..." :search="{min: 3}" :fetch="fetch"></ml-select>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    return {
      remote: [],
      fetch: function (query, page) {
        var url = new URL('https://api.github.com/search/repositories')
        var params = {q:query, page:page}
        url.search = new URLSearchParams(params).toString();
        return fetch(url).then(res => res.json()).then(result => {
          return {
            more: result.total_count > (page * 30),
            data: result.items.map(repo => {
              const {name: title, full_name: alt, id} = repo
              return {
                title, alt, id
              }
            })
          }
        })
      }
    }
  }
}
</script>
```
### Select fetch with default data

```html run
<template>
  <div>
    <ml-row>
      <ml-col>
        <ml-select :data="defaultValue" v-model="remote" style="width: 400px;" min-search="1" prop="id" multiple placeholder="Promise Options..." :search="{min: 3}" :fetch="fetch"></ml-select>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    return {
      remote: [
        'abc'
      ],
      defaultValue: [
        {
          id: 'abc',
          text: 'ABC'
        },
        {
          id: 'def',
          text: 'DEF'
        }
      ],
      fetch: function (query, page) {
        var url = new URL('https://api.github.com/search/repositories')
        var params = {q:query, page:page}
        url.search = new URLSearchParams(params).toString();
        return fetch(url).then(res => res.json()).then(result => {
          return {
            more: result.total_count > (page * 30),
            data: result.items.map(repo => {
              const {name: title, full_name: alt, id} = repo
              return {
                title, alt, id
              }
            })
          }
        })
      }
    }
  }
}
</script>
```
