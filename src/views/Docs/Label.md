# Labels

Scope and ID prop

```html run
<template>
  <div>
    <ml-label title="First Name">
      <ml-input placeholder="Placeholder" slot-scope="scope" :id="scope.id"></ml-input>
    </ml-label>
    <ml-label title="Last Name" for="user-name">
      <ml-input placeholder="Placeholder" id="user-name"></ml-input>
    </ml-label>
  </div>
</template>
```

Vertical and Horizontal

```html run
<template>
  <div>
    <ml-label title="First Name">
      <ml-input placeholder="Placeholder"></ml-input>
    </ml-label>
    <ml-label width="120px" horizontal title="Last Name">
      <ml-input placeholder="Placeholder"></ml-input>
    </ml-label>
    <ml-label width="240px" horizontal title="Last Name">
      <ml-input placeholder="Placeholder"></ml-input>
    </ml-label>
  </div>
</template>
```

Required and Icon:

```html run
<template>
  <div>
    <ml-label title="User Code" horizontal width="150px" required>
      <ml-input placeholder="Placeholder" slot-scope="scope" :id="scope.id"></ml-input>
    </ml-label>
    <ml-label title="User Code" horizontal width="150px" icon="info-sign">
      <template slot-scope="scope">
        <ml-input style="margin-bottom: 10px;" placeholder="Placeholder" :id="scope.id"></ml-input>
      </template>
    </ml-label>
  </div>
</template>
```

Custom Title Slot
```html run
<template>
  <div>
    <ml-label horizontal width="150px">
      <template slot="title">
        Mock 规则
        <ml-popover minimal placement="top">
          <template slot="target">
            <ml-button circle type="subtle" intent="primary" icon="help"></ml-button>
          </template>
          <div style="width: 300px; line-height: 1.5;">
            通过配置
            <a target="_blank" href="https://github.com/nuysoft/Mock/wiki/Syntax-Specification">
            Mock 的生成规则、属性值
            </a>，通过 Mock Server 生成指定数据。
            <br>
            <br>
            更多 Mock 示例请访问 <a target="_blank" href="http://mockjs.com/examples.html">Mock.js 的示例页面</a>
          </div>
        </ml-popover>
      </template>
      <template slot-scope="scope">
        <ml-input style="margin-bottom: 10px;" placeholder="Placeholder" :id="scope.id"></ml-input>
      </template>
    </ml-label>
  </div>
</template>

