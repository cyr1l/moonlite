# Tab

```html run
<template>
  <div class="align-left">
    <ml-tab type="card">
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
    </ml-tab>
    <ml-tab type="card">
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
    </ml-tab>
    <ml-tab type="default">
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
    </ml-tab>
    <ml-tab type="default">
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
    </ml-tab>
    <ml-tab slim type="default">
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
    </ml-tab>
    <p>Custom title slot:</p>
    <ml-tab type="default">
      <template slot="title" slot-scope="scope">
        {{scope.data.title}}<ml-icon :name="scope.data.icon"></ml-icon>
      </template>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
    </ml-tab>
    <p>extra slot:</p>
    <ml-tab type="card">
      <template slot="extra-right">
        <ml-button intent="primary">rightExtraBtn</ml-button>
      </template>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
    </ml-tab>
    <p>left extra slot:</p>
    <ml-tab type="card">
      <template slot="extra-left">
        <ml-button intent="primary">leftExtraBtn</ml-button>
      </template>
      <ml-tab-item title="航班情况" icon="airplane">
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
    </ml-tab>
  </div>
</template>
```
```html run
<template>
  <div>
    <ml-button @click="modalVisible = true" icon="applications" type="primary">
      模态窗口标签页
    </ml-button>
    <ml-modal neat :visible.sync="modalVisible">
      <ml-tab type="default">
        <ml-tab-item title="航班情况" icon="airplane">
          <template>
            Flight Info
          </template>
        </ml-tab-item>
        <ml-tab-item title="应用分析" icon="application">
          Application Analyze
        </ml-tab-item>
        <ml-tab-item title="快捷键" icon="key-command">
          Short Cuts
        </ml-tab-item>
      </ml-tab>
    </ml-modal>
  </div>
</template>
<script>
export default {
  name: 'Tab',
  data () {
    return {
      modalVisible: false
    }
  },
  methods: {
  },
  components: {
  }
}
</script>
```
```html run
<template>
  <div>
    <ml-row class="align-left" :gutter="10">
      <ml-col :span="12">
        <ml-row :gutter="10">
          <ml-col :span="8">
            <ml-tab type="default" position="top">
              <ml-tab-item title="航班情况" icon="airplane">
                <template>
                  Flight Info
                </template>
              </ml-tab-item>
              <ml-tab-item title="应用分析" icon="application">
                Application Analyze
              </ml-tab-item>
              <ml-tab-item title="快捷键" icon="key-command">
                Short Cuts
              </ml-tab-item>
            </ml-tab>
          </ml-col>
          <ml-col :span="4">
            <ml-tab type="default" position="right">
              <ml-tab-item title="航班情况" icon="airplane">
                <template>
                  Flight Info
                </template>
              </ml-tab-item>
              <ml-tab-item title="应用分析" icon="application">
                Application Analyze
              </ml-tab-item>
              <ml-tab-item title="快捷键" icon="key-command">
                Short Cuts
              </ml-tab-item>
            </ml-tab>
          </ml-col>
          <ml-col :span="4">
            <ml-tab type="default" position="left">
              <ml-tab-item title="航班情况" icon="airplane">
                <template>
                  Flight Info
                </template>
              </ml-tab-item>
              <ml-tab-item title="应用分析" icon="application">
                Application Analyze
              </ml-tab-item>
              <ml-tab-item title="快捷键" icon="key-command">
                Short Cuts
              </ml-tab-item>
            </ml-tab>
          </ml-col>
          <ml-col :span="8">
            <ml-tab type="default" position="bottom">
              <ml-tab-item title="航班情况" icon="airplane">
                <template>
                  Flight Info
                </template>
              </ml-tab-item>
              <ml-tab-item title="应用分析" icon="application">
                Application Analyze
              </ml-tab-item>
              <ml-tab-item title="快捷键" icon="key-command">
                Short Cuts
              </ml-tab-item>
            </ml-tab>
          </ml-col>
        </ml-row>
      </ml-col>
      <ml-col :span="12">
      <hr class="ml-hr">
      </ml-col>
      <ml-col :span="12">
        <ml-row :gutter="10">
          <ml-col :span="8">
            <ml-tab type="card" position="top">
              <ml-tab-item title="航班情况" icon="airplane">
                <template>
                  Flight Info
                </template>
              </ml-tab-item>
              <ml-tab-item title="应用分析" icon="application">
                Application Analyze
              </ml-tab-item>
              <ml-tab-item title="快捷键" icon="key-command">
                Short Cuts
              </ml-tab-item>
            </ml-tab>
          </ml-col>
          <ml-col :span="4">
            <ml-tab type="card" position="right">
              <ml-tab-item title="航班情况" icon="airplane">
                <template>
                  Flight Info
                </template>
              </ml-tab-item>
              <ml-tab-item title="应用分析" icon="application">
                Application Analyze
              </ml-tab-item>
              <ml-tab-item title="快捷键" icon="key-command">
                Short Cuts
              </ml-tab-item>
            </ml-tab>
          </ml-col>
          <ml-col :span="4">
            <ml-tab type="card" position="left">
              <ml-tab-item title="航班情况" icon="airplane">
                <template>
                  Flight Info
                </template>
              </ml-tab-item>
              <ml-tab-item title="应用分析" icon="application">
                Application Analyze
              </ml-tab-item>
              <ml-tab-item title="快捷键" icon="key-command">
                Short Cuts
              </ml-tab-item>
            </ml-tab>
          </ml-col>
          <ml-col :span="8">
            <ml-tab type="card" position="bottom">
              <ml-tab-item title="航班情况" icon="airplane">
                <template>
                  Flight Info
                </template>
              </ml-tab-item>
              <ml-tab-item title="应用分析" icon="application">
                Application Analyze
              </ml-tab-item>
              <ml-tab-item title="快捷键" icon="key-command">
                Short Cuts
              </ml-tab-item>
            </ml-tab>
          </ml-col>
        </ml-row>
      </ml-col>
    </ml-row>
  </div>
</template>
```

```html run
<template>
  <div class="align-left">
    <ml-row>
      <ml-col :span="12">
        <ml-tab type="default" animate currentIndex=2>
          <ml-tab-item title="航班情况" icon="airplane">
            <template>
              Flight Info
            </template>
          </ml-tab-item>
          <ml-tab-item title="应用分析" icon="application">
            Application Analyze
          </ml-tab-item>
          <ml-tab-item title="快捷键" icon="key-command">
            Short Cuts
          </ml-tab-item>
        </ml-tab>
      </ml-col>
    </ml-row>
    <ml-tab type="default">
      <ml-tab-item @init="$message('Flight Initialized')" title="航班情况" icon="airplane" slim>
        <template>
          Flight Info
        </template>
      </ml-tab-item>
      <ml-tab-item @init="$message('Application Initialized')" title="应用分析" icon="application">
        Application Analyze
      </ml-tab-item>
      <ml-tab-item @init="$message('ShortCut Initialized')" title="快捷键" icon="key-command">
        Short Cuts
      </ml-tab-item>
    </ml-tab>
    <ml-tab type="default">
      <ml-tab-item v-for="(tab, idx) in data" :title="tab" icon="airplane" v-if="idxs[idx]" :key="idx">
        <template>
          {{tab}}
        </template>
      </ml-tab-item>
    </ml-tab>
    <ml-tab type="default">
      <ml-tab-item title="wtf?">
        <template>
          ???
        </template>
      </ml-tab-item>
      <ml-tab-item title="wtf??">
        <template>
          ???
        </template>
      </ml-tab-item>
      <ml-tab-item title="wtf???" v-if="show">
        <template>
          ???
        </template>
      </ml-tab-item>
    </ml-tab>
    <ml-tab v-model="active">
      <ml-tab-item title="One" name="one">
        <template>
          1.One
        </template>
      </ml-tab-item>
      <ml-tab-item title="Two" name="two">
        <template>
          2.Two
        </template>
      </ml-tab-item>
      <ml-tab-item title="Three" name="three">
        <template>
          3.Three
        </template>
      </ml-tab-item>
    </ml-tab>
    <code>{{active}}</code>
  </div>
</template>
<script>
export default {
  name: 'Tab',
  data () {
    return {
      idxs: [true, true, true, true, true],
      data: [
        'Zero',
        'One',
        'Two',
        'Three',
        'Four'
      ],
      active: 'two',
      show: false
    }
  },
  watch: {
    active: {
      handler: function (x) {
        console.log('active', x)
      }
    }
  },
  mounted () {
    window.setTimeout(() => {
      this.idxs = [true, true, false, true, true],
      window.setTimeout(() => {
        this.idxs = [true, true, true, true, true]
        window.setTimeout(() => {
          this.idxs = [false, false, false, false, true]
          this.show = true
        }, 2000)
      }, 2000)
    }, 3000)
  }
}
</script>
```