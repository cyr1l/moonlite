# BreadCrumb

Breadcrumb navigation:

Indicate the current page’s location depth using a navigation list that automatically adds separators using CSS.


Default Separator: /
```html run
<template>
  <ml-row>
    <ml-col>
      <ml-breadcrumb>
        <ml-breadcrumb-item :to="{path: '/1'}">
          Folder one
        </ml-breadcrumb-item>
        <ml-breadcrumb-item :to="{path: '/2'}">
          Folder two
        </ml-breadcrumb-item>
        <ml-breadcrumb-item :to="{path: '/2'}">
          Folder three
        </ml-breadcrumb-item>
        <ml-breadcrumb-item>
          File
        </ml-breadcrumb-item>
      </ml-breadcrumb>
    </ml-col>
  </ml-row>
</template>
```

Custom Separator: 
```html run
<template>
  <ml-row>
    <ml-col>
      <ml-breadcrumb separator=">">
        <ml-breadcrumb-item :to="{path: '/1'}">
          Folder one
        </ml-breadcrumb-item>
        <ml-breadcrumb-item :to="{path: '/2'}">
          Folder two
        </ml-breadcrumb-item>
        <ml-breadcrumb-item :to="{path: '/2'}">
          Folder three
        </ml-breadcrumb-item>
        <ml-breadcrumb-item>
          File
        </ml-breadcrumb-item>
      </ml-breadcrumb>

      <ml-breadcrumb separator-icon="chevron-right">
        <ml-breadcrumb-item :to="{path: '/'}">
          Folder one
        </ml-breadcrumb-item>
        <ml-breadcrumb-item :to="{path: '/'}">
          Folder two
        </ml-breadcrumb-item>
        <ml-breadcrumb-item :to="{path: '/'}">
          Folder three
        </ml-breadcrumb-item>
        <ml-breadcrumb-item>
          File
        </ml-breadcrumb-item>
      </ml-breadcrumb>

      <ml-breadcrumb separator-icon="double-chevron-right">
        <ml-breadcrumb-item :to="{path: '/'}">
          Folder one
        </ml-breadcrumb-item>
        <ml-breadcrumb-item :to="{path: '/'}">
          Folder two
        </ml-breadcrumb-item>
        <ml-breadcrumb-item :to="{path: '/'}">
          Folder three
        </ml-breadcrumb-item>
        <ml-breadcrumb-item>
          File
        </ml-breadcrumb-item>
      </ml-breadcrumb>
    </ml-col>
  </ml-row>
</template>
```
