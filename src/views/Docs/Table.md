# Table

Table is a table.

### Default
```html run
<template>
  <div>
    <ml-table :data="tableData">
      <ml-table-column prop="args" label="参数" width="120px"></ml-table-column>
      <ml-table-column prop="description" label="参数描述" width="30%"></ml-table-column>
      <ml-table-column label="类型" width="20%">
        <template slot-scope="scope">
          {{scope.data.type}}
        </template>
      </ml-table-column>
      <ml-table-column label="默认值" prop="default" width="20%">
        <template slot-scope="scope">
          {{scope.$index}}
        </template>
      </ml-table-column>
    </ml-table>
  </div>
</template>
<script>
export default {
  data () {
    return {
      tableData: [
        {
          args: 'title',
          description: 'desc',
          type: 'type3',
          default: 'default'
        },
        {
          args: 'title2',
          description: 'desc2',
          type: 'type2',
          default: 'default2'
        },
        {
          args: 'title3',
          description: 'desc3',
          type: 'type1',
          default: 'default3'
        },
        {
          args: 'title4',
          description: 'desc4',
          type: 'type4',
          default: 'default4'
        }
      ]
    }
  }
}
</script>
```

### Thin
```html run
<template>
  <div>
    <ml-radio-group v-model="className">
      <ml-radio value="ml-table-fat">
        Fat
      </ml-radio>
      <ml-radio value="">
        默认
      </ml-radio>
      <ml-radio value="ml-table-thin">
        Thin
      </ml-radio>
    </ml-radio-group>
    <ml-table :data="tableData" :class="className">
      <ml-table-column prop="args" label="参数" width="120px"></ml-table-column>
      <ml-table-column prop="description" label="参数描述" width="30%"></ml-table-column>
      <ml-table-column label="类型" width="20%">
        <template slot-scope="scope">
          {{scope.data.type}}
        </template>
      </ml-table-column>
      <ml-table-column label="默认值" prop="default" width="20%">
        <template slot-scope="scope">
          {{scope.$index}}
        </template>
      </ml-table-column>
    </ml-table>
  </div>
</template>
<script>
export default {
  data () {
    return {
      className: 'ml-table-fat',
      tableData: [
        {
          args: 'title',
          description: 'desc',
          type: 'type3',
          default: 'default'
        },
        {
          args: 'title2',
          description: 'desc2',
          type: 'type2',
          default: 'default2'
        },
        {
          args: 'title3',
          description: 'desc3',
          type: 'type1',
          default: 'default3'
        },
        {
          args: 'title4',
          description: 'desc4',
          type: 'type4',
          default: 'default4'
        }
      ]
    }
  }
}
</script>
```


### Style
```html run
<template>
  <div>
    <ml-switch v-model="bordered" on-text="Border Style"></ml-switch>
    <br>
    <ml-switch v-model="interactive" on-text="Interactive: (Hover style)"></ml-switch>
    <br>
    <ml-switch v-model="checkable" on-text="With checkbox"></ml-switch>
    <ml-table :data="tableData" :interactive="interactive" :bordered="bordered" :with-checkbox="checkable" @selection="updateSelection">
      <ml-table-column prop="args" label="参数" width="120px"></ml-table-column>
      <ml-table-column prop="description" label="参数描述" width="30%"></ml-table-column>
      <ml-table-column label="类型" width="20%">
        <template slot-scope="scope">
          {{scope.data.type}}
        </template>
      </ml-table-column>
      <ml-table-column label="默认值" prop="default" width="20%">
        <template slot-scope="scope">
          {{scope.$index}}
        </template>
      </ml-table-column>
    </ml-table>
    <p>当前已选择了 {{selectedCount}} 个</p>
  </div>
</template>
<script>
export default {
  data () {
    return {
      selectedCount: 0,
      interactive: false,
      bordered: false,
      checkable: false,
      tableData: [
        {
          args: 'title',
          description: 'desc',
          type: 'type3',
          default: 'default'
        },
        {
          args: 'title2',
          description: 'desc2',
          type: 'type2',
          default: 'default2'
        },
        {
          args: 'title3',
          description: 'desc3',
          type: 'type1',
          default: 'default3'
        },
        {
          args: 'title4',
          description: 'desc4',
          type: 'type4',
          default: 'default4'
        }
      ]
    }
  },
  methods: {
    updateSelection (data) {
      this.selectedCount = data.length
    }
  }
}
</script>
```


### Sortable
```html run
<template>
  <div>
    <ml-label title="Sort Icon:" width="120px" horizontal>
      <ml-select prop="id" v-model="sortIcon">
        <option value="chevron">chevron type</option>
        <option value="list">list type</option>
      </ml-select>
    </ml-label>
    
    <ml-table :data="tableData" bordered :sort-icon="sortIcon">
      <ml-table-column prop="args" label="参数" width="120px"></ml-table-column>
      <ml-table-column prop="description" label="参数描述" width="30%"></ml-table-column>
      <ml-table-column label="类型" width="20%">
        <template slot-scope="scope">
          {{scope.data.type}}
        </template>
      </ml-table-column>
      <ml-table-column label="默认值" prop="default" width="20%"></ml-table-column>
    </ml-table>
  </div>
</template>
<script>
export default {
  data () {
    return {
      sortIcon: 'chevron',
      tableData: [
        {
          args: 'title',
          description: 'desc',
          type: 'type3',
          default: 'default'
        },
        {
          args: 'title2',
          description: 'desc2',
          type: 'type2',
          default: 'default2'
        },
        {
          args: 'title3',
          description: 'desc3',
          type: 'type1',
          default: 'default3'
        },
        {
          args: 'title4',
          description: 'desc4',
          type: 'type4',
          default: 'default4'
        }
      ]
    }
  }
}
</script>
```



### Custom Empty State
```html run
<template>
  <div>
    <ml-table :data="[]" sort-icon="list">
      <ml-table-column prop="args" label="参数" width="120px"></ml-table-column>
      <ml-table-column prop="description" label="参数描述" width="30%"></ml-table-column>
      <ml-table-column label="类型" width="20%">
        <template slot-scope="scope">
          {{scope.data.type}}
        </template>
      </ml-table-column>
      <ml-table-column label="默认值" prop="default" width="20%"></ml-table-column>
      <template slot="empty">
        <div class="align-center">
          <ml-icon name="issue"></ml-icon>
          暂无数据
        </div>
      </template>
    </ml-table>
  </div>
</template>
```
