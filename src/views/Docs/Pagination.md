# Pagination

Pagenation is used to provide pagination

```html run
<template>
  <div class="align-left">
    <ml-pagination></ml-pagination>
    <hr class="ml-hr">
    <ml-pagination
      @pagination="pagination"
      :page="page"
      :size="limit"
      :total="total"></ml-pagination>
    <hr class="ml-hr">
    <ml-pagination
      @pagination="pagination"
      intent="danger"
      :page="page"
      :size="limit"
      :total="total"></ml-pagination>
    <hr class="ml-hr">
    <ml-pagination
      @pagination="pagination"
      intent="warning"
      type="hollow"
      :page="page"
      :size="limit"
      :total="total"></ml-pagination>
    <hr class="ml-hr">
    <pre class="logs"><code v-for="(log, $idx) in logs" :key="$idx">{{log}}<br></code></pre>
  </div>
</template>

<script>
export default {
  data () {
    return {
      page: 2,
      limit: 10,
      total: 100,
      logs: []
    }
  },
  methods: {
    pagination (page) {
      this.page = page
      this.logs.unshift(
        'Go to page: ' + page
      )
    }
  }
}
</script>

<style lang="stylus">
@import '~@/stylesheets/vars'
.logs {
  padding: 10px;
  background: $dark;
  color: $white;
  font-size: $fontSize;
  max-height: 300px;
  overflow: auto;
}
</style>
```
