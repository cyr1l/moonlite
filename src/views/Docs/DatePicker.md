# Datepicker

```html run
<template>
  <div>
    <ml-row>
      <ml-col :span="12">
        <ml-datepicker precision="minute" v-model="date0"></ml-datepicker>
        <ml-datepicker v-model="date"></ml-datepicker>
        <ml-datepicker v-model="date1"></ml-datepicker>
        <hr class="ml-hr">
        <ml-datepicker format="YYYY-MM-DD" type="time" v-model="date2"></ml-datepicker>
        <ml-datepicker format="YYYY-MM-DD" append type="time" v-model="date3"></ml-datepicker>
        <hr class="ml-hr">
        <ml-datepicker v-model="datepicker"></ml-datepicker>
        <ml-datepicker precision="second" style="width: 320px" range v-model="datepickerrange"></ml-datepicker>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    const HOUR = 60 * 60 * 1000
    const DAY = 24 * HOUR
    // const MONTH = 30 * DAY
    return {
      date0: new Date('2021-01-12 12:23:34'),
      date: new Date() - DAY,
      date1: new Date() - HOUR,
      date2: new Date(),
      date3: new Date(),
      datepicker: new Date(),
      datepickerrange: [new Date(), new Date()]
    }
  }
}
</script>
```
