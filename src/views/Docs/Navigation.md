# Navigation

### Basic 
```html run
<template>
  <div>
    <ml-row>
      <ml-col :offset="3" :span="6">
        <ml-navigation :height="height" :theme="currentTheme" :fill="fill" :collapse="collapse">
          <ml-nav-item exact icon="home" :to="{name: 'home'}">
            项目首页
          </ml-nav-item>
          <ml-nav-group title="组件列表">
            <ml-nav-item icon="layout-grid" :to="{name: 'Modal'}">
              模态窗口 Modal
            </ml-nav-item>
            <ml-nav-item icon="layout-grid" :to="{name: 'Pagination'}">
              分页 Pagination
            </ml-nav-item>
            <ml-nav-item icon="layout-grid" :to="{name: 'Progress'}">
              进度 Progress
            </ml-nav-item>
          </ml-nav-group>
          <ml-nav-group title="表单组件">
            <ml-nav-item icon="layout-grid" :to="{name: 'Input'}">
              输入框 Input
            </ml-nav-item>
            <ml-nav-item icon="layout-grid" :to="{name: 'Label'}">
              标签 Label
            </ml-nav-item>
          </ml-nav-group>
          <ml-nav-group title="Normal Links">
            <ml-nav-item icon="arrow-right" href="http://www.google.com" append>
              Go to Google
            </ml-nav-item>
            <ml-nav-item icon="link" href="http://www.google.com" target="_blank" append>
              In New Tab
            </ml-nav-item>
          </ml-nav-group>
        </ml-navigation>
      </ml-col>
    </ml-row>
    <div style="margin-top: 30px;" >
      <ml-button @click="collapse = !collapse" :icon="collapse ? 'chevron-forward' : 'chevron-backward'">Toggle Collapse</ml-button>
      <ml-button icon="arrows-horizontal" data-tooltip="Notice the hover state" @click="fill = !fill">Fill</ml-button>
      <ml-button icon="tint" @click="currentTheme = currentTheme ? undefined : theme ">Toggle Theme</ml-button>
      <ml-button icon="tint" @click="height = (height === '600px') ? '300px' : '600px' ">Toggle Height</ml-button>
    </div>
    
  </div>
</template>

<script>
export default {
  data () {
    return {
      collapse: false,
      fill: false,
      currentTheme: undefined,
      theme: {
        primary: '#fff',
        secondary: 'rgb(241, 211, 209)',
        background: 'rgb(189, 43, 37)',
        active: 'rgb(196, 76, 71)',
        hover: 'rgb(201, 91, 87)',
        selected: 'rgb(155, 35, 30)'
      },
      height: '600px'
    }
  }
}
</script>
```

### Themes: 

```html run
<template>
  <div>
    <ml-row>
      <ml-col width="300px" v-for="(theme, $idx) in themes" :key="$idx" style="margin-bottom: 20px;">
        <ml-navigation fill :collapse="collapse" :theme="theme">
          <ml-nav-item exact icon="home" :to="{name: 'home'}">
            项目首页
          </ml-nav-item>
          <ml-nav-group title="组件列表" :collapsable="false">
            <ml-nav-item icon="layout-grid" :to="{name: 'Modal'}">
              模态窗口 Modal
            </ml-nav-item>
            <ml-nav-item icon="layout-grid" :to="{name: 'Pagination'}">
              分页 Pagination
            </ml-nav-item>
            <ml-nav-item icon="layout-grid" :to="{name: 'Progress'}">
              进度 Progress
            </ml-nav-item>
          </ml-nav-group>
          <ml-nav-group title="表单组件" :collapsable="false">
            <ml-nav-item icon="layout-grid" :to="{name: 'Input'}">
              输入框 Input
            </ml-nav-item>
            <ml-nav-item icon="menu" :to="{name: 'Navigation'}">
              侧栏导航 Navigation
            </ml-nav-item>
          </ml-nav-group>
        </ml-navigation>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
  data () {
    return {
      themes: [
        {
          primary: '#fff',
          secondary: 'rgb(221, 219, 240)',
          background: 'rgb(85, 72, 177)',
          active: 'rgb(96, 84, 185)',
          hover: 'rgb(110, 98, 191)',
          selected: 'rgb(68, 54, 164)'
        },
        {
          primary: 'rgb(171, 187, 214)',
          secondary: 'rgb(105, 119, 143)',
          background: 'rgb(14, 22, 36)',
          active: 'rgb(27, 40, 64)',
          hover: 'rgb(37, 58, 95)',
          selected: 'rgb(36, 50, 76)'
        },
        {
          primary: '#fff',
          secondary: 'rgb(241, 211, 209)',
          background: 'rgb(189, 43, 37)',
          active: 'rgb(196, 76, 71)',
          hover: 'rgb(201, 91, 87)',
          selected: 'rgb(155, 35, 30)'
        },
        {
          primary: '#fff',
          secondary: 'rgb(241, 211, 209)',
          background: 'rgb(70, 158, 239)',
          active: 'rgb(120, 177, 229)',
          hover: 'rgb(138, 187, 233)',
          selected: 'rgb(32, 137, 236)'
        },
        {
          primary: '#000',
          secondary: 'rgb(110, 88, 31);',
          background: 'rgb(246, 197, 68)',
          active: 'rgb(235, 203, 119)',
          hover: 'rgb(238, 210, 137)',
          selected: 'rgb(244, 185, 29)'
        },
        {
          primary: '#fff',
          secondary: 'rgb(221, 219, 240)',
          background: 'linear-gradient(150deg, rgb(85, 72, 177) 0%, rgb(145, 73, 182) 100%)',
          active: 'rgb(96, 84, 185)',
          hover: 'rgb(110, 98, 191)',
          selected: 'rgb(68, 54, 164)'
        }
      ],
      gradientTheme: {
        primary: '#fff',
        secondary: 'rgb(221, 219, 240)',
        background: 'linear-gradient(150deg, rgb(85, 72, 177) 0%, rgb(145, 73, 182) 100%)',
        active: 'rgb(96, 84, 185)',
        hover: 'rgb(110, 98, 191)',
        selected: 'rgb(68, 54, 164)'
      }
    }
  }
}
</script>
```


### Nested children item:

```html run
<template>
  <div>
    <ml-row>
      <ml-col width="300px">
        <ml-nav fill :collapse="collapse">
          <ml-nav-item exact icon="home" :to="{name: 'home'}">
            项目首页
          </ml-nav-item>
          <ml-nav-group title="组件列表" :collapsable="false">
            <ml-nav-item icon="layout-grid" placement="right-end" interact="click">
              模态窗口 Modal
              <template slot="children">
                <ml-nav-item icon="layout-grid" :to="{name: 'Modal'}">
                  模态窗口 Modal
                </ml-nav-item>
                <ml-nav-item icon="layout-grid" :to="{name: 'Pagination'}">
                  分页 Pagination
                </ml-nav-item>
                <ml-nav-item icon="layout-grid" placement="right-start" interact="click">
                  模态窗口 Modal
                  <template slot="children">
                    <ml-nav-item icon="layout-grid" :to="{name: 'Modal'}">
                      模态窗口 Modal
                    </ml-nav-item>
                    <ml-nav-item icon="layout-grid" :to="{name: 'Pagination'}">
                      分页 Pagination
                    </ml-nav-item>
                    <ml-nav-item icon="layout-grid" :to="{name: 'Progress'}">
                      进度 Progress
                    </ml-nav-item>
                  </template>
                </ml-nav-item>
                <ml-nav-item icon="layout-grid" :to="{name: 'Progress'}">
                  进度 Progress
                </ml-nav-item>
              </template>
            </ml-nav-item>
            <ml-nav-item icon="layout-grid" :to="{name: 'Pagination'}">
              分页 Pagination
            </ml-nav-item>
            <ml-nav-item icon="layout-grid" :to="{name: 'Progress'}">
              进度 Progress
            </ml-nav-item>
          </ml-nav-group>
          <ml-nav-group title="表单组件" :collapsable="false">
            <ml-nav-item icon="layout-grid" :to="{name: 'Input'}">
              输入框 Input
            </ml-nav-item>
            <ml-nav-item icon="menu" :to="{name: 'Navigation'}">
              侧栏导航 Navigation
            </ml-nav-item>
            <ml-nav-item icon="align-left" placement="right-start" interact="click">
              子导航项目
              <template slot="children">
                <ml-nav-group title="子项目列表" :collapsable="false">
                  <ml-nav-item icon="layout-grid" :to="{name: 'Modal'}">
                    模态窗口 Modal
                  </ml-nav-item>
                  <ml-nav-item icon="layout-grid" :to="{name: 'Pagination'}">
                    分页 Pagination
                  </ml-nav-item>
                  <ml-nav-item icon="layout-grid" :to="{name: 'Progress'}">
                    进度 Progress
                  </ml-nav-item>
                </ml-nav-group>
              </template>
            </ml-nav-item>
          </ml-nav-group>
        </ml-nav>
      </ml-col>
    </ml-row>
  </div>
</template>
```