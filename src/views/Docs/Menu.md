# Menu

### Basic

```html run
<template>
  <div>
    <ml-menu>
      <ml-menu-item text="Select text" icon="select"></ml-menu-item>
      <ml-menu-item text="New text box" icon="new-text-box"></ml-menu-item>
      <ml-menu-item text="New object" icon="new-object"></ml-menu-item>
      <ml-menu-item text="New Link" icon="new-link"></ml-menu-item>
      <ml-menu-divider></ml-menu-divider>
      <ml-menu-item text="Settings..." icon="cog">
        <template slot="alt">
          ⌘C
        </template>
      </ml-menu-item>
    </ml-menu>

    <ml-menu style="margin-left: 100px;">
      <ml-menu-divider title="Edit"></ml-menu-divider>
      <ml-menu-item text="Cut" icon="cut" label="⌘X"></ml-menu-item>
      <ml-menu-item text="Copy" icon="duplicate" label="⌘C"></ml-menu-item>
      <ml-menu-item text="Paste" icon="clipboard" label="⌘V"></ml-menu-item>
      <ml-menu-divider title="Text"></ml-menu-divider>
      <ml-menu-item icon="align-left" text="Alignment" interact="click">
        <template slot="children">
          <ml-menu-item icon="align-left" text="Left"></ml-menu-item>
          <ml-menu-item icon="align-center" text="Center"></ml-menu-item>
          <ml-menu-item icon="align-right" text="Right"></ml-menu-item>
          <ml-menu-item icon="align-justify" text="Justify"></ml-menu-item>
        </template>
      </ml-menu-item>
      <ml-menu-item icon="style" text="Style">
        <template slot="children" slot-scope="scope">
          <ml-menu-item icon="bold" text="Bold" @click="scope.popover.close()" />
          <ml-menu-item icon="italic" text="Italic" />
          <ml-menu-item icon="underline" text="Underline" />
        </template>
      </ml-menu-item>
      <ml-menu-item icon="asterisk" text="Miscellaneous">
        <template slot="children">
          <ml-menu-item icon="badge" text="Badge" />
          <ml-menu-item icon="book" text="Long items will truncate when they reach max-width" />
          <ml-menu-item icon="more" text="Look in here for even more items">
            <template slot="children">
              <ml-menu-item icon="briefcase" text="Briefcase" />
              <ml-menu-item icon="calculator" text="Calculator" />
              <ml-menu-item icon="dollar" text="Dollar" />
              <ml-menu-item icon="dot" text="Shapes">
                <template slot="children">
                  <ml-menu-item icon="full-circle" text="Full circle" />
                  <ml-menu-item icon="heart" text="Heart" />
                  <ml-menu-item icon="ring" text="Ring" />
                  <ml-menu-item icon="square" text="Square" />
                </template>
              </ml-menu-item>
            </template>
          </ml-menu-item>
        </template>
      </ml-menu-item>
    </ml-menu>
  </div>
</template>
```

```html run
<template>
  <div>
    <ml-popover thin minimal :arrow="false" placement="bottom-start">
      <ml-menu no-shadow>
        <ml-menu-divider title="Edit"></ml-menu-divider>
        <ml-menu-item text="Cut" icon="cut" label="⌘X"></ml-menu-item>
        <ml-menu-item text="Copy" icon="duplicate" label="⌘C"></ml-menu-item>
        <ml-menu-item text="Paste" icon="clipboard" label="⌘V"></ml-menu-item>
        <ml-menu-divider title="Text"></ml-menu-divider>
        <ml-menu-item icon="align-left" text="Alignment"></ml-menu-item>
        <ml-menu-item icon="style" text="Style"></ml-menu-item>
        <ml-menu-item icon="asterisk" text="Miscellaneous"></ml-menu-item>
      </ml-menu>
      <template slot="target">
        <ml-button intent="primary" icon="caret-down" append>下拉菜单</ml-button>
      </template>
    </ml-popover>
    <ml-popover style="margin-left: 100px;" thin minimal placement="bottom-start">
      <ml-menu no-shadow>
        <ml-menu-divider title="Edit"></ml-menu-divider>
        <ml-menu-item text="Cut" icon="cut" label="⌘X"></ml-menu-item>
        <ml-menu-item text="Copy" icon="duplicate" label="⌘C"></ml-menu-item>
        <ml-menu-item text="Paste" icon="clipboard" label="⌘V"></ml-menu-item>
        <ml-menu-divider title="Text"></ml-menu-divider>
        <ml-menu-item icon="align-left" text="Alignment"></ml-menu-item>
        <ml-menu-item icon="style" text="Style"></ml-menu-item>
        <ml-menu-item icon="asterisk" text="Miscellaneous"></ml-menu-item>
      </ml-menu>
      <template slot="target">
        <ml-button intent="primary" icon="caret-down" append>下拉菜单</ml-button>
      </template>
    </ml-popover>
    <ml-popover style="margin-left: 100px;" thin placement="bottom-start">
      <ml-menu no-shadow>
        <ml-menu-divider title="Edit"></ml-menu-divider>
        <ml-menu-item text="Cut" icon="cut" label="⌘X"></ml-menu-item>
        <ml-menu-item text="Copy" icon="duplicate" label="⌘C"></ml-menu-item>
        <ml-menu-item text="Paste" icon="clipboard" label="⌘V"></ml-menu-item>
        <ml-menu-divider title="Text"></ml-menu-divider>
        <ml-menu-item icon="align-left" text="Alignment"></ml-menu-item>
        <ml-menu-item icon="style" text="Style"></ml-menu-item>
        <ml-menu-item icon="asterisk" text="Miscellaneous"></ml-menu-item>
      </ml-menu>
      <template slot="target">
        <ml-button intent="primary" icon="caret-down" append>下拉菜单</ml-button>
      </template>
    </ml-popover>
  </div>
</template>
```

```html run
<template>
  <div>
    <div class="context-menu-area" @contextmenu.prevent="rightMenu($event)">
      Right click here.
      <br>
      请在此右键
    </div>
    <ml-menu :style="contextMenuStyle">
      <ml-menu-divider title="Edit"></ml-menu-divider>
      <ml-menu-item text="Cut" icon="cut" label="⌘X"></ml-menu-item>
      <ml-menu-item text="Copy" icon="duplicate" label="⌘C"></ml-menu-item>
      <ml-menu-item text="Paste" icon="clipboard" label="⌘V"></ml-menu-item>
      <ml-menu-divider title="Text"></ml-menu-divider>
      <ml-menu-item icon="align-left" text="Alignment">
        <template slot="children">
          <ml-menu-item icon="align-left" text="Left"></ml-menu-item>
          <ml-menu-item icon="align-center" text="Center"></ml-menu-item>
          <ml-menu-item icon="align-right" text="Right"></ml-menu-item>
          <ml-menu-item icon="align-justify" text="Justify"></ml-menu-item>
        </template>
      </ml-menu-item>
      <ml-menu-item icon="style" text="Style">
        <template slot="children" slot-scope="scope">
          <ml-menu-item icon="bold" text="Bold" @click="scope.popover.close()" />
          <ml-menu-item icon="italic" text="Italic" />
          <ml-menu-item icon="underline" text="Underline" />
        </template>
      </ml-menu-item>
      <ml-menu-item icon="asterisk" text="Miscellaneous">
        <template slot="children">
          <ml-menu-item icon="badge" text="Badge" />
          <ml-menu-item icon="book" text="Long items will truncate when they reach max-width" />
          <ml-menu-item icon="more" text="Look in here for even more items">
            <template slot="children">
              <ml-menu-item icon="briefcase" text="Briefcase" />
              <ml-menu-item icon="calculator" text="Calculator" />
              <ml-menu-item icon="dollar" text="Dollar" />
              <ml-menu-item icon="dot" text="Shapes">
                <template slot="children">
                  <ml-menu-item icon="full-circle" text="Full circle" />
                  <ml-menu-item icon="heart" text="Heart" />
                  <ml-menu-item icon="ring" text="Ring" />
                  <ml-menu-item icon="square" text="Square" />
                </template>
              </ml-menu-item>
            </template>
          </ml-menu-item>
        </template>
      </ml-menu-item>
    </ml-menu>
  </div>
</template>

<script>
export default {
  data () {
    return {
      contextMenuVisible: false,
      contextMenuStyle: {
        display: 'none'
      }
    }
  },
  mounted () {
    window.document.addEventListener('click', () => {
      this.contextMenuStyle = {
        display: 'none'
      }
    }, false)
  },
  methods: {
    clickAlert () {
      this.$message('Hello')
    },
    rightMenu ($event) {
      console.log("Right Menu", $event)
      this.contextMenuVisible = true
      this.contextMenuStyle = {
        display: 'block',
        position: 'fixed',
        left: $event.clientX + 'px',
        top: $event.clientY + 'px'
      }
    }
  }
}
</script>

<style lang="stylus" scoped>
@import '~@/stylesheets/vars'
.context-menu-area {
  padding: 20px;
  width: 500px;
  max-width: 100%;
  height: 300px;
  text-align: center;
  background: $bright;
  bdrs(2px)
  margin: 40px auto;
  border: 1px solid $light;
  display: flex;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  color: $secondary;
}
</style>
```