---
title: Layout
styles: ./styles/styl
date: 2019-09-20
---

# Layout

## Grid System
<ml-row>
  <ml-col :span="3">
    <div class="fill-block"></div>
  </ml-col>
  <ml-col :span="3">
    <div class="fill-block secondary"></div>
  </ml-col>
  <ml-col :span="3">
    <div class="fill-block gray"></div>
  </ml-col>
  <ml-col :span="3">
    <div class="fill-block light"></div>
  </ml-col>
</ml-row>

```html run
<template>
  <div>
    <h1>
      Layout
    </h1>
    <p>
      span = 4
    </p>
    <ml-row>
      <ml-col :span="4">
        <div class="fill-block"></div>
      </ml-col>
      <ml-col :span="4">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="4">
        <div class="fill-block gray"></div>
      </ml-col>
    </ml-row>
    <p>
      span = 3
    </p>
    <ml-row>
      <ml-col :span="3">
        <div class="fill-block"></div>
      </ml-col>
      <ml-col :span="3">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="3">
        <div class="fill-block gray"></div>
      </ml-col>
      <ml-col :span="3">
        <div class="fill-block light"></div>
      </ml-col>
    </ml-row>
    <p>
      span = 2
    </p>
    <ml-row>
      <ml-col :span="2">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="2">
        <div class="fill-block gray"></div>
      </ml-col>
      <ml-col :span="2">
        <div class="fill-block light"></div>
      </ml-col>
      <ml-col :span="2">
        <div class="fill-block dark"></div>
      </ml-col>
      <ml-col :span="2">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="2">
        <div class="fill-block gray"></div>
      </ml-col>
    </ml-row>
    <p>
      span = 3, gutter = 10
    </p>
    <ml-row :gutter="10">
      <ml-col :span="3">
        <div class="fill-block"></div>
      </ml-col>
      <ml-col :span="3">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="3">
        <div class="fill-block gray"></div>
      </ml-col>
      <ml-col :span="3">
        <div class="fill-block light"></div>
      </ml-col>
    </ml-row>
    <p>
      span = 3, with offset = 2
    </p>
    <ml-row>
      <ml-col :span="3">
        <div class="fill-block"></div>
      </ml-col>
      <ml-col :span="3" :offset="2">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="3">
        <div class="fill-block gray"></div>
      </ml-col>
    </ml-row>
    <p>Responsive:</p>
    <p>
      with xs, sm, md, lg, xlg
    </p>
    <ml-row>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="6" :xs="12">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="6" :xs="12">
        <div class="fill-block gray"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="6" :xs="12">
        <div class="fill-block light"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="6" :xs="12">
        <div class="fill-block dark"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="6" :xs="12">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="6" :xs="12">
        <div class="fill-block gray"></div>
      </ml-col>
    </ml-row>
    <p>
      with xs, sm, md, lg, xlg, hide
    </p>
    <ml-row>
      <ml-col :span="2" :xlg="0" :lg="3" :ml="4" :sm="6" :xs="12">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="0" :ml="4" :sm="6" :xs="12">
        <div class="fill-block gray"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="0" :sm="6" :xs="12">
        <div class="fill-block light"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="0" :xs="12">
        <div class="fill-block dark"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="6" :xs="0">
        <div class="fill-block secondary"></div>
      </ml-col>
      <ml-col :span="2" :xlg="2" :lg="3" :ml="4" :sm="6" :xs="12">
        <div class="fill-block gray"></div>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
export default {
}
</script>

<style lang="stylus">
@import '~@/stylesheets/vars'
.fill-block {
  width: 100%;
  height: 40px;
  background: $dark;
  margin-bottom: 10px;
  // bdrs(3px)
  &.secondary {
    background: $secondary
  }
  &.gray {
    background: $gray
  }
  &.light {
    background: $light
  }
}
</style>
```