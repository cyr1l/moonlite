import ModalShot from './Modal.vue'
import dom from '../utils/dom'

class Modal {
  constructor (options, MLModalShot, slots) {
    if (typeof options === 'string') {
      options = {
        message: options
      }
    }
    this.options = options
    this.options.visible = false
    // this.options.portal = false
    this.modal = new MLModalShot({ propsData: options, slots: slots })
  }

  static container () {
    var $container = document.querySelector('.ml-portal-container')
    if (!$container) {
      $container = dom.template('<div class="ml-portal-container"></div>')[0]
      dom.append($container, document.body)
    }
    return $container
  }

  show () {
    this.modal.$mount()
    // let modalDom = this.modal.$el
    // dom.append(modalDom, Modal.container())
    this.modal.show()
    this.modal.$on('closed', () => {
      dom.remove(this.modal.$el)
      this.modal.$destroy()
    })
    return this.modal
  }
}

class MLException extends Error {
  constructor (message) {
    super(message)
    this.message = message
    this.name = 'MLError'
  }
  toString () {
    return `[${this.name}]: ${this.message}`
  }
}

function install (Vue) {
  var MLModalShot = Vue.extend(ModalShot)

  Vue.prototype.$modal = function (opt) {
    return new Modal(opt, MLModalShot).show()
  }

  Vue.prototype.$alert = function (opt) {
    return new Promise((resolve, reject) => {
      var modal
      if (typeof opt === 'string') {
        opt = {
          title: ' ',
          message: opt
        }
      }
      var confirm = opt.confirm || {}
      var actions = [
        {
          text: confirm.text || 'OK',
          icon: confirm.icon || 'tick',
          type: confirm.type || 'primary',
          focus: typeof confirm.focus === 'boolean' ? confirm.focus : true,
          dismiss:
            typeof confirm.dismiss === 'boolean' ? confirm.dismiss : true,
          callback: function () {
            resolve(modal)
          }
        }
      ]
      opt.actions = actions
      modal = new Modal(opt, MLModalShot).show()
      modal.$on('close', function () {
        reject(new MLException('Modal Closed'))
      })
    })
  }

  Vue.prototype.$confirm = function (opt) {
    return new Promise((resolve, reject) => {
      var modal
      var confirm = opt.confirm || {}
      var cancel = opt.cancel || {}
      var actions = [
        {
          text: confirm.text || 'Confirm',
          icon: confirm.icon || 'tick',
          type: confirm.type || 'primary',
          focus: typeof confirm.focus === 'boolean' ? confirm.focus : true,
          dismiss:
            typeof confirm.dismiss === 'boolean' ? confirm.dismiss : true,
          callback: function () {
            resolve(modal)
          }
        },
        {
          text: cancel.text || 'Cancel',
          icon: cancel.icon || '',
          type: cancel.type || 'link',
          dismiss: typeof cancel.dismiss === 'boolean' ? cancel.dismiss : true,
          callback: function () {
            reject(new MLException('Modal Canceled'))
          }
        }
      ]
      opt.actions = actions
      modal = new Modal(opt, MLModalShot).show()
      modal.$on('close', function () {
        reject(new MLException('Modal Closed'))
      })
    })
  }

  Vue.prototype.$prompt = function (opt) {
    return new Promise((resolve, reject) => {
      var modal
      var confirm = opt.confirm || {}
      var cancel = opt.cancel || {}
      var actions = [
        {
          text: confirm.text || 'Confirm',
          icon: confirm.icon || 'tick',
          type: confirm.type || 'primary',
          dismiss:
            typeof confirm.dismiss === 'boolean' ? confirm.dismiss : true,
          callback: function () {
            modal.onConfirm()
            // resolve(modal)
          }
        },
        {
          text: cancel.text || 'Cancel',
          icon: cancel.icon || '',
          type: cancel.type || 'link',
          dismiss: typeof cancel.dismiss === 'boolean' ? cancel.dismiss : true,
          callback: function () {
            reject(new MLException('Modal Canceled'))
          }
        }
      ]
      opt.actions = actions
      opt.prompt = true
      modal = new Modal(opt, MLModalShot).show()
      modal.$on('prompt', function (val) {
        resolve(val)
        if (confirm.dismiss) {
          modal.close()
        }
      })
      modal.$on('close', function () {
        reject(new MLException('Modal Closed'))
      })
    })
  }
}

export default {
  install
}
