import dom from '../utils/dom'
function update (el, binding, vnode) {
  if (vnode && vnode.componentInstance && vnode.componentInstance.$options && vnode.componentInstance.$options.name === 'MLModal') {
    vnode.componentInstance.localLoading = binding.value
    return
  }
  if (!el || !(el instanceof Element)) {
    return
  }
  var position = el.style.position || document.defaultView.getComputedStyle(el)['position']
  if (!position || position === 'static' || position === 'relative') {
    dom.addClass(el, 'ml-loading-parent-relative')
  }
  var height = dom.height(el)
  var loadingMask = Array.from(el.children).find(e => {
    return e.classList.contains('ml-loading-mask')
  })
  if (loadingMask) {
    dom.toggleClass(loadingMask, 'show', binding.value)
    dom.toggleClass(loadingMask, 'small', height < 60)
  }
}

export default {
  directive: {
    install: function (Vue) {
      Vue.directive('loading', {
        el: '',
        bind: function (el, binding, vnode) {
          if (
            vnode &&
            vnode.componentInstance &&
            vnode.componentInstance.$options &&
            vnode.componentInstance.$options.name === 'MLModal'
          ) {
            vnode.componentInstance.localLoading = binding.value
            return
          }
          var $loading = `<svg viewBox="0 0 50 50" class="spinner">
            <circle fill="none" class="ring" cx="25" cy="25" r="22.5" />
            <circle fill="none" class="line" cx="25" cy="25" r="22.5" />
          </svg>`
          if (binding.modifiers.md) {
            $loading = `<svg class="loader" width="28" height="28" viewBox="0 0 24 24" style="vertical-align:middle;">
              <path d="M12,2V6C13.66,6 15.16,6.67 16.24,7.76L19.07,4.93C17.26,3.12 14.76,2 12,2Z"></path>
              <path d="M18,12H22C22,9.24 20.88,6.74 19.07,4.93L16.24,7.76C17.33,8.84 18,10.34 18,12Z"></path>
              <path d="M22,12H18C18,13.66 17.33,15.16 16.24,16.24L19.07,19.07C20.88,17.26 22,14.76 22,12Z"></path>
              <path d="M12,18V22C14.76,22 17.26,20.88 19.07,19.07L16.24,16.24C15.16,17.33 13.66,18 12,18Z"></path>
              <path d="M12,18C10.34,18 8.84,17.33 7.76,16.24L4.93,19.07C6.74,20.88 9.24,22 12,22V18Z"></path>
              <path d="M6,12H2C2,14.76 3.12,17.26 4.93,19.07L7.76,16.24C6.67,15.16 6,13.66 6,12Z"></path>
              <path d="M2,12H6C6,10.34 6.67,8.84 7.76,7.76L4.93,4.93C3.12,6.74 2,9.24 2,12Z"></path>
              <path d="M12,6V2C9.24,2 6.74,3.12 4.93,4.93L7.76,7.76C8.84,6.67 10.34,6 12,6Z"></path>
            </svg>`
          }
          var elem = dom.template(
            `<div class="ml-loading-mask" style="">
              <div class="ml-loading-spinner">
                ${$loading}
                <div class="ml-loading-text">
                  Loading...
                </div>
              </div>
            </div>`
          )
          dom.append(elem[0], el)
        },
        inserted: function (el, binding, vnode) {
          update(el, binding, vnode)
        },
        update: function (el, binding, vnode) {
          if (binding.value !== binding.oldValue) {
            update(el, binding, vnode)
          }
        },
        unbind: function (el) {
          if (el && el.querySelector) {
            dom.remove(el.querySelector('.ml-loading-mask'))
          }
        }
      })
    }
  },
  service: {
    install: function (Vue) {
      Vue.prototype.$loading = function () {}
    }
  }
}
