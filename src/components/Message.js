// import Vue from 'vue'
import MessageShot from './Message-Shot.vue'
import dom from '../utils/dom'

class Message {
  constructor (options, MLMessageShot) {
    if (typeof options === 'string') {
      options = {
        message: options
      }
    }
    if (!options.icon) {
      switch (options.type) {
        case 'success':
          options.icon = 'tick-circle'
          break
        case 'warning':
          options.icon = 'warning-sign'
          break
        case 'error':
          options.icon = 'error'
          options.type = 'danger'
          break
        case 'info':
          options.icon = 'info-sign'
          break
        case 'primary':
          options.icon = 'issue'
          break
        default:
          options.icon = 'issue'
          break
      }
    }
    this.options = options

    this._message = new MLMessageShot({ propsData: options })
  }

  static container () {
    var $container = document.querySelector('.ml-message-container')
    if (!$container) {
      $container = dom.template('<div class="ml-message-container"></div>')[0]
      dom.append($container, document.body)
    }
    return $container
  }

  show () {
    this._message.$mount()
    dom.append(this._message.$el, Message.container())
    this._message.$on('close', () => {
    })
    this._message.$on('closed', () => {
      this._message.$destroy()
      dom.remove(this._message.$el)
    })
    return this
  }
}

function install (Vue) {
  var MLMessageShot = Vue.extend(MessageShot)
  Vue.prototype.$message = function (opt) {
    return new Message(opt, MLMessageShot).show()
  }
}

export default {
  MessageShot,
  install
}
