/**
 * Get target DOM Node
 * @param {(Node|string|Boolean)} [node=document.body] DOM Node, CSS selector, or Boolean
 * @return {Node} The target that the el will be appended to
 */

import dom from '@/utils/dom'
import _ from '@/utils/utils'

function getTarget (node = document.body) {
  let container
  if (node === true) {
    container = document.body
  } else {
    container = node instanceof window.Node ? node : document.querySelector(node)
  }
  // container
  // let $container = container.childNodes
  let $container = _.find(container.childNodes, child => {
    return dom.hasClass(child, 'ml-portal-container')
  })
  if (!$container) {
    $container = dom.template('<div class="ml-portal-container"></div>')[0]
    dom.append($container, container)
  }
  return $container
}

const homes = new Map()

const directive = {
  inserted (el, { value }, vnode) {
    const { parentNode } = el
    const home = document.createComment('')
    let hasMovedOut = false
    let targetNode = getTarget(value)
    if (value !== false && parentNode) {
      parentNode.replaceChild(home, el) // moving out, el is no longer in the document
      targetNode.appendChild(el) // moving into new place
      hasMovedOut = true
    }

    if (!homes.has(el)) homes.set(el, { parentNode, home, hasMovedOut, targetNode }) // remember where home is or should be
  },
  componentUpdated (el, binding) { // need to make sure children are done updating (vs. `update`)
    let value = binding.value
    const { parentNode, home, hasMovedOut, targetNode } = homes.get(el) // recall where home is
    let newTargetNode = getTarget(value)
    if (!hasMovedOut && value) {
      // remove from document and leave placeholder
      parentNode.replaceChild(home, el)
      // append to target
      newTargetNode.appendChild(el)

      homes.set(el, Object.assign({}, homes.get(el), { hasMovedOut: true }))
    } else if (hasMovedOut && value === false) {
      // previously moved, coming back home
      parentNode.replaceChild(el, home)
      homes.set(el, Object.assign({}, homes.get(el), { hasMovedOut: false }))
    } else if (value && targetNode !== newTargetNode) {
      // already moved, going somewhere else
      newTargetNode.appendChild(el)
    }
  },
  unbind (el, binding) {
    homes.delete(el)
  }
}

function plugin (Vue, { name = 'portal' } = {}) {
  Vue.directive(name, directive)
}

plugin.version = '0.1.6'

export default plugin

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(plugin)
}
