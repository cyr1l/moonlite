import svgIcons from '@/data/icons/icons.json'

var svgs = svgIcons.map((icon) => {
  return icon.iconName
})
var svgMap = {}

const svgNS = 'http://www.w3.org/2000/svg'
const svg = document.createElementNS(svgNS, 'svg')
svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg')
svg.setAttribute('xmlns:link', 'http://www.w3.org/1999/xlink')
svg.setAttribute('aria-hidden', 'true')
svg.id = '__SVG_SPRITE_NODE__'
svg.style.position = 'absolute'
svg.style.width = 0
svg.style.height = 0
document.body.prepend(svg)
var size = '20px'
svgs.forEach((icon) => {
  svgMap[icon] = {}
  var iconPath = require(`./svg/${size}/${icon}.svg`)
  svgMap[icon] = {
    name: icon,
    path: iconPath,
    mounted: false
  }
})

export default svgMap
