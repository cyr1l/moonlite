import MLButton from './src/components/Button'
import MLInput from './src/components/Input'
import MLSelectRaw from './src/components/SelectRaw'
import MLLabel from './src/components/Label'
import MLRow from './src/components/Row'
import MLCol from './src/components/Col'
import MLAvatar from './src/components/Avatar'
import MLSelect2 from './src/components/Select2'
import MLSelect from './src/components/Select'
import MLPagination from './src/components/Pagination'
import MLInformation from './src/components/Information'
import MLSwitch from './src/components/switch'
import MLProgress from './src/components/Progress'
import Message from './src/components/Message'
import MLModal from './src/components/Modal'
import MLDatePicker from './src/components/DatePicker/index'
import TimePicker from '@/components/DatePicker/time-picker'
import MLBreadCrumb from './src/components/BreadCrumb/index'
import MLBreadCrumbItem from './src/components/BreadCrumb/item'
import MLTable from './src/components/Table/Table'
import MLTableColumn from './src/components/Table/TableColumn'
import MLNavitaion from './src/components/Navigation/Navigation'
import MLNavItem from './src/components/Navigation/NavItem'
import MLNavGroup from './src/components/Navigation/NavGroup'
import MLTab from './src/components/Tabs/Tab'
import MLTabItem from './src/components/Tabs/TabItem'
import MLTag from './src/components/Tag'
import MLCheckbox from './src/components/Checkbox'
import MLRadio from './src/components/Radio'
import MLRadioGroup from './src/components/RadioGroup'
import MLIcon from './src/components/Icon'
import MLPopover from './src/components/Popover/Popover'
import MLMenu from './src/components/Menu/Menu'
import MLMenuItem from './src/components/Menu/MenuItem'
import MLMenuDivider from './src/components/Menu/MenuDivider'
import MLPortal from './src/components/Portal'
import MLModalService from './src/components/Modal.service'
import MLLoading from './src/components/Loading'
import MLPortalService from './src/components/Portal.service'
import MLTree from './src/components/Tree'
import MLTree2 from './src/components/Tree2'
import MLSlider from './src/components/Slider'
import MLDrawer from './src/components/Drawer'

import '@/stylesheets/main.styl'

import _ from '@/utils/utils'
const MLNav = _.cloneDeep(MLNavitaion)
MLNav.name = 'ml-nav'
const components = [
  MLButton,
  MLInput,
  MLSelectRaw,
  MLLabel,
  MLRow,
  MLCol,
  MLAvatar,
  MLSelect2,
  MLSelect,
  MLPagination,
  MLInformation,
  MLSwitch,
  MLProgress,
  MLModal,
  TimePicker,
  MLDatePicker,
  MLBreadCrumb,
  MLBreadCrumbItem,
  MLTable,
  MLTableColumn,
  MLNavitaion,
  MLNav,
  MLNavItem,
  MLNavGroup,
  MLTab,
  MLTabItem,
  MLTag,
  MLCheckbox,
  MLRadio,
  MLRadioGroup,
  MLIcon,
  MLPopover,
  MLMenu,
  MLMenuItem,
  MLMenuDivider,
  MLTree,
  MLTree2,
  MLPortal,
  MLSlider,
  MLDrawer
]

const install = function (Vue, opts = {}) {
  components.forEach(component => {
    var name = component.name.replace(/^ML/, 'ml-').toLowerCase()
    Vue.component(name, component)
  })
  Vue.use(Message)
  Vue.use(MLLoading.directive)
  Vue.use(MLModalService)
  Vue.use(MLPortalService)
}

/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  version: '0.0.1',
  install,
  MLButton,
  MLInput,
  MLSelectRaw,
  MLLabel,
  MLRow,
  MLCol,
  MLAvatar,
  MLSelect2,
  MLSelect,
  MLPagination,
  MLInformation,
  MLSwitch,
  MLProgress,
  MLModal,
  TimePicker,
  MLDatePicker,
  MLBreadCrumb,
  MLBreadCrumbItem,
  MLTable,
  MLTableColumn,
  MLNavitaion,
  MLNav,
  MLNavItem,
  MLNavGroup,
  MLTab,
  MLTabItem,
  MLTag,
  MLCheckbox,
  MLRadio,
  MLRadioGroup,
  MLIcon,
  MLPopover,
  MLMenu,
  MLMenuItem,
  MLMenuDivider,
  MLTree,
  MLTree2,
  MLPortal,
  MLSlider,
  MLDrawer
}
