# MoonLite Changelog


## [v0.0.69](https://bitbucket.org/cyr1l/moonlite/src/v0.0.69)

> 2021-02-19

### Features

* Added Slider Support.


## [v0.0.68](https://bitbucket.org/cyr1l/moonlite/src/v0.0.68)

> 2021-01-06

### Features

* TimePicker support, Remove useless icons.


## [v0.0.67](https://bitbucket.org/cyr1l/moonlite/src/v0.0.67)

> 2020-12-24

### Bug Fixes

* Tab `value` update while setup.


## [v0.0.66](https://bitbucket.org/cyr1l/moonlite/src/v0.0.66)

> 2020-12-24

### Features

* Select clearable.


## [v0.0.65](https://bitbucket.org/cyr1l/moonlite/src/v0.0.65)

> 2020-12-17

### Bug Fixes

* Modal Loading.


## [v0.0.64](https://bitbucket.org/cyr1l/moonlite/src/v0.0.64)

> 2020-12-10

### Bug Fixes

* popover destroy


## [v0.0.63](https://bitbucket.org/cyr1l/moonlite/src/v0.0.63)

> 2020-11-26

### Bug Fixes

* select value could be number.


## [v0.0.62](https://bitbucket.org/cyr1l/moonlite/src/v0.0.62)

> 2020-11-23


## [v0.0.61](https://bitbucket.org/cyr1l/moonlite/src/v0.0.61)

> 2020-11-17


## [v0.0.60](https://bitbucket.org/cyr1l/moonlite/src/v0.0.60)

> 2020-10-29


## [v0.0.59](https://bitbucket.org/cyr1l/moonlite/src/v0.0.59)

> 2020-10-27


## [v0.0.58](https://bitbucket.org/cyr1l/moonlite/src/v0.0.58)

> 2020-10-27


## [v0.0.57](https://bitbucket.org/cyr1l/moonlite/src/v0.0.57)

> 2020-10-27


## [v0.0.56](https://bitbucket.org/cyr1l/moonlite/src/v0.0.56)

> 2020-10-21

### Bug Fixes

* Tree performance issue.


## [v0.0.55](https://bitbucket.org/cyr1l/moonlite/src/v0.0.55)

> 2020-10-14

### Bug Fixes

* Default Value.
* ESLint.

### Features

* Icon more efficient way.


## [v0.0.54](https://bitbucket.org/cyr1l/moonlite/src/v0.0.54)

> 2020-08-31

### Bug Fixes

* Nested Label Style.


## [v0.0.53](https://bitbucket.org/cyr1l/moonlite/src/v0.0.53)

> 2020-08-24


## [v0.0.52](https://bitbucket.org/cyr1l/moonlite/src/v0.0.52)

> 2020-07-27

### Bug Fixes

* Select: Remote, Props, Template ordered.


## [v0.0.51](https://bitbucket.org/cyr1l/moonlite/src/v0.0.51)

> 2020-07-08

### Bug Fixes

* value could trigger update props.


## [v0.0.50](https://bitbucket.org/cyr1l/moonlite/src/v0.0.50)

> 2020-07-08

### Bug Fixes

* Select Remote Compare.


## [v0.0.49](https://bitbucket.org/cyr1l/moonlite/src/v0.0.49)

> 2020-07-08

### Features

* Remote Select could keep options when query updated.


## [v0.0.48](https://bitbucket.org/cyr1l/moonlite/src/v0.0.48)

> 2020-06-24

### Features

* Select Option could be disabled.


## [v0.0.45](https://bitbucket.org/cyr1l/moonlite/src/v0.0.45)

> 2020-04-27

### Bug Fixes

* Lodash Requirements, ESLint.


## [v0.0.44](https://bitbucket.org/cyr1l/moonlite/src/v0.0.44)

> 2020-04-22

### Bug Fixes

* Select unobserve null element.


## [v0.0.43](https://bitbucket.org/cyr1l/moonlite/src/v0.0.43)

> 2020-03-31

### Bug Fixes

* Fix Tree Sidebar slot template.


## [v0.0.42](https://bitbucket.org/cyr1l/moonlite/src/v0.0.42)

> 2020-03-31


## [v0.0.41](https://bitbucket.org/cyr1l/moonlite/src/v0.0.41)

> 2020-03-31

### Features

* Tree refactor.


## [v0.0.40](https://bitbucket.org/cyr1l/moonlite/src/v0.0.40)

> 2020-03-31


## [v0.0.39](https://bitbucket.org/cyr1l/moonlite/src/v0.0.39)

> 2019-12-19

### Features

* Fix loading mask. Get Nearest Loading Mask.


## [v0.0.38](https://bitbucket.org/cyr1l/moonlite/src/v0.0.38)

> 2019-12-03

### Bug Fixes

* tab item should be avaliable to push into tabs.

### Features

* Tab support v-model with item name.
* Added Fetch Option for Select.
* Added TOC on docs.

### Features

* Added Table of contents for documents.


## [v0.0.34](https://bitbucket.org/cyr1l/moonlite/src/v0.0.34)

> 2019-08-19


## [v0.0.33](https://bitbucket.org/cyr1l/moonlite/src/v0.0.33)

> 2019-07-29

### Bug Fixes

* tab item should be avaliable to push into tabs.


## [v0.0.32](https://bitbucket.org/cyr1l/moonlite/src/v0.0.32)

> 2019-07-23

### Features

* Avatar can accept slot.


## [v0.0.31](https://bitbucket.org/cyr1l/moonlite/src/v0.0.31)

> 2019-07-23

### Bug Fixes

* Navigation Router Link update.


## [v0.0.30](https://bitbucket.org/cyr1l/moonlite/src/v0.0.30)

> 2019-07-23

### Bug Fixes

* Tab Highlight position when tab item toggle.


## [v0.0.29](https://bitbucket.org/cyr1l/moonlite/src/v0.0.29)

> 2019-07-18

### Features

* Added Context Menu Demo.


## [v0.0.28](https://bitbucket.org/cyr1l/moonlite/src/v0.0.28)

> 2019-07-16


## [v0.0.27](https://bitbucket.org/cyr1l/moonlite/src/v0.0.27)

> 2019-06-21

### Bug Fixes

* fix slot option initial value.


## [v0.0.26](https://bitbucket.org/cyr1l/moonlite/src/v0.0.26)

> 2019-06-21

### Features

* Select accept option as opts.


## [v0.0.25](https://bitbucket.org/cyr1l/moonlite/src/v0.0.25)

> 2019-06-20

### Bug Fixes

* select options wrapper minWidth update when open.


## [v0.0.24](https://bitbucket.org/cyr1l/moonlite/src/v0.0.24)

> 2019-06-08

### Features

* Label add title slot and Input add Number Support


## [v0.0.23](https://bitbucket.org/cyr1l/moonlite/src/v0.0.23)

> 2019-05-31


## [v0.0.22](https://bitbucket.org/cyr1l/moonlite/src/v0.0.22)

> 2019-05-28

### Bug Fixes

* Select2 Multiple update data after value.


## [v0.0.21](https://bitbucket.org/cyr1l/moonlite/src/v0.0.21)

> 2019-05-28


## [v0.0.20](https://bitbucket.org/cyr1l/moonlite/src/v0.0.20)

> 2019-05-28


## [v0.0.19](https://bitbucket.org/cyr1l/moonlite/src/v0.0.19)

> 2019-05-24

### Features

* Updated Modal Doc.


## [v0.0.18](https://bitbucket.org/cyr1l/moonlite/src/v0.0.18)

> 2019-04-16


## [v0.0.17](https://bitbucket.org/cyr1l/moonlite/src/v0.0.17)

> 2019-04-10

### Bug Fixes

* Flex Layout specified `width` prop.


## [v0.0.16](https://bitbucket.org/cyr1l/moonlite/src/v0.0.16)

> 2019-04-09

### Features

* Grid System updated to flexbox.


## [v0.0.15](https://bitbucket.org/cyr1l/moonlite/src/v0.0.15)

> 2019-03-26

### Bug Fixes

* Select Model Default Empty and Delayed Options data.

### Features

* Added Button Docs and Button Square Prop.


## [v0.0.14](https://bitbucket.org/cyr1l/moonlite/src/v0.0.14)

> 2019-03-19

### Bug Fixes

* Prevent Select2 Emit input event twice.


## [0.0.13](https://bitbucket.org/cyr1l/moonlite/src/0.0.13)

> 2019-03-19

### Bug Fixes

* Can close modal via esc key if only closable.

### Features

* Added select event for select2.
* Modal can close via escape key.


## [v0.0.11](https://bitbucket.org/cyr1l/moonlite/src/v0.0.11)

> 2019-03-15


## [0.0.10](https://bitbucket.org/cyr1l/moonlite/src/0.0.10)

> 2019-03-15

### Bug Fixes

* Fixed eslint for Markdown file.

### Features

* Select2 Support Remote Options.
* Added Prop Property for Select.


## [0.0.9](https://bitbucket.org/cyr1l/moonlite/src/0.0.9)

> 2019-03-02

### Bug Fixes

* Restored Selecte2.

### Features

* Added Native ML-Select Support.(Deprecated Select2)


## [0.0.8](https://bitbucket.org/cyr1l/moonlite/src/0.0.8)

> 2019-02-28

### Features

*  Added Tree Support.


## [0.0.7](https://bitbucket.org/cyr1l/moonlite/src/0.0.7)

> 2019-02-14

### Bug Fixes

* Select Disabled Attributes.


## [0.0.6](https://bitbucket.org/cyr1l/moonlite/src/0.0.6)

> 2019-02-14

### Bug Fixes

* remove id in svg, added semantic support.


## [0.0.5](https://bitbucket.org/cyr1l/moonlite/src/0.0.5)

> 2019-02-13

### Bug Fixes

* Enhance Modal and Message Event Handler.
* dismiss Popover with `ml-popover-dismiss` class

### Features

* Added information heavy props.
* Added Navigation alias and custom theme support.
* Added Popover and Menu Component.


## [0.0.4](https://bitbucket.org/cyr1l/moonlite/src/0.0.4)

> 2019-02-01

### Bug Fixes

* Fixed RadioGroup checked state. Feature: Added Layout hide support.

### Features

* Navigation Item Active State Auto Detect.
* Added Popover Support.
* added Underline Input style..


## [0.0.3](https://bitbucket.org/cyr1l/moonlite/src/0.0.3)

> 2019-01-08

### Features

* Added Table Checkbox support.


## [0.0.1](https://bitbucket.org/cyr1l/moonlite/src/0.0.1)

> 2018-12-28

### Declaration

* Initialized Project.

### Features

* added closable prop for modal

