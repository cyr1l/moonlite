# MoonLite

### What is MoonLite

Moonlite is a Vue componets library which focused on customization and design.

### Getting Start

You can checkout our documents and demos at [docs](#TODO)

### Install

```shell
npm install moonlite --save
```

Then in your `Vue` file:
```html
<template>
  <div>
    <ml-button></ml-button>
  </div>
</template>

<script>
import MLButton from 'moonlite'
export default {
  data () {
    return {
      message: 'Hello world!'
    }
  },
  components: {
    MLButton
  }
}
<script>
```

### Contribute

You are welcome to contribute this repo!

### Licence

MIT