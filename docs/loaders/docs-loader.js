const parser = require('./parser')
const fs = require('fs')
const path = require('path')
const { getOptions, interpolateName } = require('loader-utils')

function saveCacheFile (example, option) {
  let tempDirectory = option.tempDirectory
  return Promise.all(
    example.map(e => {
      return new Promise((resolve, reject) => {
        let filePath = interpolateName(this, `[name].[ext].[hash]`, {
          content: e.code
        })
        let fileTarget = path.resolve(tempDirectory, filePath)
        let fileTargetWithExtension = `${fileTarget}.vue`
        try {
          fs.accessSync(fileTargetWithExtension)
          fs.unlinkSync(fileTargetWithExtension)
        } catch (err) {}
        fs.writeFile(fileTargetWithExtension, e.code, err => {
          if (err) {
            reject(err)
          } else {
            resolve({ fileTarget, ...e })
          }
        })
      })
    })
  )
}

function build (markdown, option) {
  let examples = markdown.examples
  let examplesMapString = examples
    .map(example => {
      return `${example.name}: \`${example.template}\``
    })
    .join(',\n        ')
  let tableOfContent = JSON.stringify(markdown.toc)
  return saveCacheFile
    .bind(this)(
      examples.filter(e => {
        return e.runnable
      }),
      option
    )
    .then(examples => {
      let importString = examples
        .map(e => {
          return `import ${e.name} from '${e.fileTarget}'`
        })
        .join('\n')
      let componentsString = examples
        .map(e => {
          return e.name
        })
        .join(',\n  ')
      let result = `<template>
  <div>
    <ml-row gutter="10">
      <ml-col :xs="12" :sm="12" :md="10" :lg="10" :xlg="10">
        <div class="page-content">
          ${markdown.html}
        </div>
      </ml-col>
      <ml-col :xs="0" :sm="0" :md="2" :lg="2" :xlg="2">
        <aside class="table-of-content">
          <ml-toc :data="toc"></ml-toc>
        </aside>
      </ml-col>
    </ml-row>
  </div>
</template>

<script>
/* eslint-disable */
${importString}

let exampleComponents = {
  ${componentsString}
}

export default {
  data () {
    return {
      exampleMap: {${examplesMapString}},
      toc: ${tableOfContent}
    }
  },
  components: exampleComponents
}
</script>

<style lang="stylus">
</style>
`
      let filePath = interpolateName(
        this,
        `__RESULT__[name].[ext].[hash].vue`,
        { content: result }
      )
      let tempDirectory = option.tempDirectory
      let fileTarget = path.resolve(tempDirectory, filePath)
      try {
        fs.accessSync(fileTarget)
        fs.unlinkSync(fileTarget)
      } catch (err) {}
      fs.writeFileSync(fileTarget, result)
      return result
    })
}

module.exports = function loader (content) {
  const callback = this.async()
  this.cacheable && this.cacheable()

  const tempDirectory = path.resolve(
    'node_modules/.cache/moonlite-markdown-loader'
  )
  const defaults = {
    tempDirectory
  }
  const options = Object.assign({}, defaults, getOptions(this))

  try {
    fs.accessSync(tempDirectory)
  } catch (err) {
    fs.mkdirSync(tempDirectory)
  }

  parser
    .parse(content)
    .then(obj => {
      return build.bind(this)(obj, options)
    })
    .then(component => callback(null, component))
    .catch(callback)
}
