const frontMatter = require('front-matter')
let marked = require('marked')
let uslug = require('uslug')

function parse (markdown) {
  return new Promise((resolve, reject) => {
    let html
    markdown = frontMatter(markdown)
    let exampleMap = []
    let toc = []
    var renderer = new marked.Renderer()
    renderer.heading = function (text, level) {
      var slug = uslug(text)
      toc.push({
        level: level,
        slug: slug,
        title: text
      })
      return `<h${level} id="${slug}"><a href="#${slug}" class="anchor"></a>${text}</h${level}>`
    }
    renderer.code = function (code, languageStr) {
      let newExampleVar = `componentName${exampleMap.length}`
      let template = code
        .replace(/\\/g, '\\\\')
        .replace(/`/g, '\\`')
        .replace(/<\/script/gi, '<\\/script')
      let [language, mode] = languageStr.split(' ')
      exampleMap.push({
        name: newExampleVar,
        runnable: mode === 'run',
        code: code,
        language,
        template: template
      })
      if (mode === 'run') {
        return `<ml-example language="${language}" :template="exampleMap['${newExampleVar}']"><${newExampleVar}/></ml-example>`
      } else {
        return `<ml-code language="${language}" :value="exampleMap['${newExampleVar}']"></ml-code>`
      }
    }
    try {
      html = marked(markdown.body, { renderer: renderer })
      return resolve({
        html,
        attributes: markdown.attributes,
        examples: exampleMap,
        toc: toc
      })
    } catch (err) {
      return reject(err)
    }
  })
}

module.exports = {
  parse
}
