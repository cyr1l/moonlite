let path = require('path')

module.exports = {
  productionSourceMap: false,
  runtimeCompiler: true,
  configureWebpack: {
    devtool: 'source-map',
    resolveLoader: {
      modules: [
        path.resolve(__dirname, 'docs/loaders')
      ]
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      }
    },
    module: {
      rules: [
        {
          test: /\.md$/,
          use: [
            {
              loader: 'cache-loader',
              options: {
                cacheDirectory: 'node_modules/.cache/moonlite-markdown-loader'
              }
            },
            { loader: 'vue-loader', options: {} },
            { loader: 'docs-loader', options: {} }
          ]
        }
      ]
    }
  },
  chainWebpack: config => {
    const svgRule = config.module.rule('svg')
    // clear all existing loaders.
    // if you don't do this, the loader below will be appended to
    // existing loaders of the rule.
    svgRule.uses.clear()

    // add replacement loader(s)
    svgRule
      .use('svg-inline-loader')
      .loader('svg-inline-loader')
      .tap(options => {
        options = options || {}
        options.removeTags = true
        options.removingTags = ['title', 'desc', 'defs', 'style']
        options.removeSVGTagAttrs = true
        options.removingTagAttrs = [
          'id',
          'viewBox',
          'version',
          'xmlns',
          'xmlns:xlink',
          'enable-background',
          'xml:space',
          'fill-rule',
          'clip-rule',
          'stroke',
          'stroke-width'
        ]
        return options
      })
  }
}
